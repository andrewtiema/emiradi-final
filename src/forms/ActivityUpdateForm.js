import React, { Component } from 'react';
import { Container, Header, Content, List, ListItem, Thumbnail, Text, Left, Body, Right, Button } from 'native-base';
export default class ActivityUpdateForm extends Component {
  render() {
    return (
      <List>
            <ListItem thumbnail>
              <Body>
                <Text>Sector</Text>
                <Text note numberOfLines={2}>Transport & Development</Text>
              </Body>
            </ListItem>
            <ListItem thumbnail>
              <Body>
                <Text>Project</Text>
                <Text note numberOfLines={2}>Rural Electrification</Text>
              </Body>
            </ListItem>
            <ListItem thumbnail>
              <Body>
                <Text>Activity Name</Text>
                <Text note numberOfLines={2}>Classroom Construction .</Text>
              </Body>
              
            </ListItem>
            <ListItem thumbnail>
              <Body>
                <Text>Activity Description</Text>
                <Text note numberOfLines={2}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</Text>
              </Body>
              
            </ListItem>
            <ListItem thumbnail>
              <Body>
                <Text>Due Date</Text>
                <Text note numberOfLines={2}>2020-12-01</Text>
              </Body>
            </ListItem>

            

            <ListItem thumbnail>
              <Body>
                <Text>Assigned To</Text>
                <Text note numberOfLines={2}>Martin Muriithi</Text>
              </Body>
            </ListItem>
            
            <ListItem thumbnail>
              <Body>
                <Text>Target Unit</Text>
                <Text note numberOfLines={2}>Class rooms built</Text>
              </Body>
            </ListItem>
            <ListItem thumbnail>
              <Body>
                <Text>Evidence Required</Text>
                <Text note numberOfLines={2}>Photos of project progress</Text>
              </Body>
            </ListItem>
            <ListItem thumbnail>
              <Body>
                <Text>Target Achieved</Text>
                <Text note numberOfLines={2}>20% Archieved</Text>
              </Body>
            </ListItem>
            <ListItem thumbnail>
              <Body>
                <Text>Last Update</Text>
                <Text note numberOfLines={2}>2020-09-12</Text>
              </Body>
            </ListItem>
            
          </List>
    );
  }
}