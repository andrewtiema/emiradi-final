import React, { Component } from 'react';
import { View, Text, StyleSheet, Button,SafeAreaView,Dimensions } from 'react-native';
import {Avatar} from 'react-native-paper';
import Icon from 'react-native-vector-icons/Ionicons';
import ActivityUpdateForm from './components/ActivityUpdateForm';
import {
  NavigationActions,
  createSwitchNavigator,
  createAppContainer
} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {createBottomTabNavigator,createTabNavigator} from 'react-navigation-tabs';
import {createDrawerNavigator} from 'react-navigation-drawer';
import { createMaterialTopTabNavigator,createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';

import DashboardScreen from './screens/DashboardScreen';
import ProjectActivitiesScreen from './screens/ProjectActivitiesScreen';
import ProjectUsersScreen from './screens/ProjectUsersScreen';
import ProfileScreen from './screens/ProfileScreen';
import ProjectDashboardScreen from './screens/ProjectDashboardScreen';
import SettingsScreen from './screens/SettingsScreen';
import ProjectMapScreen from './screens/ProjectMapScreen';
import ActivityDashboardScreen from './screens/ActivityDashboardScreen';
import ProjectsGalleryScreen from './screens/ProjectsGalleryScreen';
import ActivityUpdateScreen from './screens/ActivityUpdateScreen';
import NotificationsScreen from './screens/NotificationsScreen';
import ProjectsExploreScreen from './screens/ProjectsExploreScreen';
import ActivityReportScreen from './screens/ActivityReportScreen';
import SideBar from "./sections/SideBarSection";
import CameraScreen from './screens/CameraScreen';
import CameraImageSelectorScreen from './screens/CameraImageSelectorScreen';
import ExpoImagePickerScreen from './screens/ExpoImagePickerScreen';
import MyProfileScreen from './screens/MyProfileScreen';
import FlatListSearch from './screens/FlatListSearch';
import { DataNavigation } from 'react-data-navigation';
import {DataModel} from './screens/DataModel';
import {
  HomeScreen,
  LoginScreen,
  RegisterScreen,
  ForgotPasswordScreen,
  Dashboard,
} from './screens';
import OverallPerformancePie from './components/OverallPerformancePie';

class Profile extends Component {
  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Profile</Text>
      </View>
    );
  }
}


const AuthStack = createStackNavigator({
  /**SignIn: SignInScreen,
  ForgotPassword: ForgotPasswordScreen,**/
  HomeScreen,
  LoginScreen,
  RegisterScreen,
  ForgotPasswordScreen,
  //Dashboard,
},
{
    initialRouteName: 'HomeScreen',
    headerMode: 'none',
  }
);

const ProfileStack = createStackNavigator({
  ProfileScreen: {
    screen: ProfileScreen
  }
},
{
  
  defaultNavigationOptions: ({ navigation }) => {
    return {
      headerStyle:{backgroundColor:"#D04A02"},
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold'
      },      
      headerTitle: 'User Profile',
      headerLeft: (        
        <Icon style={{ paddingLeft: 10,color:"#fff" }} onPress={() => navigation.dispatch(NavigationActions.back())} name="md-arrow-back" size={30} />
      )
    };
  }

}
);

const MyProfileStack = createStackNavigator({
  MyProfileScreen: {
    screen: MyProfileScreen
  }
},
{
  
  defaultNavigationOptions: ({ navigation }) => {
    return {
      headerStyle:{backgroundColor:"#D04A02"},
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold'
      },      
      headerTitle: 'My Profile',
      headerLeft: (
        <Icon style={{ paddingLeft: 10,color:"#fff" }} onPress={() => navigation.openDrawer()} name="md-menu" size={30} />
      )
    };
  }

}
);

const NotificationsStack = createStackNavigator({
  NotificationsScreen: {
    screen: NotificationsScreen
  }
},
{
  
    defaultNavigationOptions: ({ navigation }) => {
      return {
        headerStyle:{backgroundColor:"#D04A02"},
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: 'bold'
        },      
        headerTitle: 'Notifications',
        headerLeft: (
          <Icon style={{ paddingLeft: 10,color:"#fff" }} onPress={() => navigation.openDrawer()} name="md-menu" size={30} />
        )
      };
    }
  
}
);

const SettingsStack = createStackNavigator({
  SettingsStack: {
    screen: SettingsScreen,
    navigationOptions: ({ navigation }) => {
      return {
        headerStyle:{backgroundColor:"#D04A02"},
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: 'bold'
        },      
        headerTitle: 'App Details',
        headerLeft:(<Icon style={{ paddingLeft: 10,color:"#fff" }} onPress={() => navigation.openDrawer()} name="md-menu" size={30} />)
        
      };
    }
  }
});

const GalleryStack = createStackNavigator({
  ProjectsGalleryScreen: {
    screen: ProjectsGalleryScreen,
    navigationOptions: ({ navigation }) => {
      return {
        headerTitle: 'Projects Gallery',
        headerLeft: (
          <Icon style={{ paddingLeft: 10,color:"#fff" }} onPress={() => navigation.openDrawer()} name="md-menu" size={30} />
        )
      };
    }
  }
});


const ActivityDashboardStack = createStackNavigator(
  {
    ActivityDashboardScreen: {
      screen: ActivityDashboardScreen,
      
      
    },
    ActivityUpdateScreen: {
      screen: ActivityUpdateScreen,
      navigationOptions:({ navigation }) => {
        return {
          headerStyle:{backgroundColor:"#D04A02"},
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold'
          },      
          headerTitle: 'Activities Update',
          //Removes Top padding on the stack header when stack is placed within another stack
          headerForceInset: { top: 'never', bottom: 'never' },
          headerLeft:("")
          
        };
      }
    },

    CameraScreen: {
      screen: ExpoImagePickerScreen,

      navigationOptions:({ navigation }) => {
        return {
          headerStyle:{backgroundColor:"#D04A02"},
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold'
          },      
          headerTitle: 'Take Photo',
          //Removes Top padding on the stack header when stack is placed within another stack
          headerForceInset: { top: 'never', bottom: 'never' },
          headerLeft:("")
          
        };
      }
    }
  },
  {
    defaultNavigationOptions: ({ navigation }) => {
      return {
        headerStyle:{backgroundColor:"#D04A02"},
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: 'bold'
        },      
        headerTitle: 'Activities Status',
        //Removes Top padding on the stack header when stack is placed within another stack
        headerForceInset: { top: 'never', bottom: 'never' },
        
      };
    }
  }
  
);

const ProjectActivitiesStack = createStackNavigator(
  {
    ProjectActivities: {
      screen: ProjectActivitiesScreen,
      
    },

    ActivityDashboardStack: {
      screen: ActivityDashboardStack,
      navigationOptions: ({ navigation }) => {
        return {
          //headerShown:false,
          headerStyle:{backgroundColor:"#D04A02"},
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold'
          },      
          headerTitle: DataNavigation.getData("activity").title,
          //Removes Top padding on the stack header when stack is placed within another stack
          headerForceInset: { top: 'never', bottom: 'never' },
          headerLeft:("")
        };
      }
    }
  },
  {
  defaultNavigationOptions: ({ navigation }) => {
    return {
      headerStyle:{backgroundColor:"#D04A02"},
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold'
      },      
      headerTitle: 'All Activities Status',
      //Removes Top padding on the stack header when stack is placed within another stack
      headerForceInset: { top: 'never', bottom: 'never' }
    };
  }
}
);



const ProjectMapStack = createStackNavigator({
  ProjectMapStack: {
    screen: ProjectMapScreen
  }
},
{
  defaultNavigationOptions: ({ navigation }) => {
    return {
      headerStyle:{backgroundColor:"#D04A02"},
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold'
      },      
      headerTitle: 'Mapped Locations',
      //Removes Top padding on the stack header when stack is placed within another stack
      headerForceInset: { top: 'never', bottom: 'never' }
    };
  }
}

);
const ProjectUsersStack = createStackNavigator({
  ProjectUsersScreen: {
    screen: ProjectUsersScreen
  },
  ProfileStack: {
    screen: ProfileStack,
    navigationOptions: ({ navigation }) => {
      return {
        headerShown:false,
        headerStyle:{backgroundColor:"#D04A02"},
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: 'bold'
        },      
        headerTitle: 'User Profile',
        //Removes Top padding on the stack header when stack is placed within another stack 
        //headerForceInset: { top: 'never', bottom: 'never' },
        headerLeft: (<Icon style={{ paddingLeft: 10,color:"#fff" }} onPress={() => navigation.goBack()} name="md-arrow-back" size={30} />)
      };
    }
  },
},
{
  defaultNavigationOptions: ({ navigation }) => {
    return {
      headerStyle:{backgroundColor:"#D04A02"},
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold'
      },      
      headerTitle: 'Project Users',
      //Removes Top padding on the stack header when stack is placed within another stack 
      headerLeft: (
        <Icon style={{ paddingLeft: 10,color:"#fff" }} onPress={() => navigation.openDrawer()} name="md-menu" size={30} />
      )
    };
  }
}

);




const ProjectDashboardStack = createStackNavigator(
  {
    ProjectDashboardScreen: {
      screen: ProjectDashboardScreen,
     
    },
    ActivityDashboardScreen: {
      screen: ActivityDashboardScreen
    }
  },
  {
    defaultNavigationOptions: ({ navigation }) => {
      return {
       header:null
      }
    }
  }
);


let activities_count=0;
if(DataNavigation.getData("project"))
{
  activities_count=3;
}
else
{
  activities_count=0;
}

const project_activities=()=>{
  return DataNavigation.getData("project")?3:0;
}

const ProjectDashboardTab = createMaterialBottomTabNavigator(
  {    
    ProjectDashboardStack:{
        'screen':ProjectDashboardStack,
        navigationOptions:{
          'tabBarLabel':'Dashboard',
          tabBarIcon:({focused,tintColor})=>{
            return <Icon name="ios-options" size={25}  color={tintColor}/>;
          }
      }
    },
    ProjectActivitiesStack:{
        'screen':ProjectActivitiesStack,
        navigationOptions:({ navigation }) => ({
        'tabBarLabel':'Activities',
        'tabBarBadge':DataNavigation.getData("activities_count"),
         tabBarIcon:({focused,tintColor})=>{
           return <Icon name="ios-list" size={25}  color={tintColor}/>;
         }
      })
    },

    ProjectMapStack:{
        'screen':ProjectMapStack,navigationOptions:{
        'tabBarLabel':'Map',
         tabBarIcon:({focused,tintColor})=>{
           return <Icon name="ios-map" size={25}  color={tintColor}/>;
         }
      }
    },
    /**ProjectUsersStack:{
      'screen':ProjectUsersStack,navigationOptions:{
      'tabBarLabel':'Users',
       tabBarIcon:({focused,tintColor})=>{
         return <Icon name="ios-people" size={25}  color={tintColor}/>;
       }
    }
  }**/
    
    
  },

  {
    initialRouteName: 'ProjectDashboardStack',
    activeColor: '#D04A02',
    inactiveColor: '#000',
    barStyle :{
            backgroundColor: '#FFF',
            fontWeight:"300"
    }
  }
);

const ProjectStack = createStackNavigator(
    {
      ProjectDashboardTab: {
        screen:ProjectDashboardTab
    },
  },
  {
    defaultNavigationOptions: ({ navigation }) => {
      return {
        headerStyle:{backgroundColor:"#D04A02"},
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: 'bold',textAlign: 'center',alignSelf:'center'
        },
        
        gesturesEnabled: false,
        headerTitle: DataNavigation.getData("project").title,
        headerLeft: (
          <Icon style={{ paddingLeft: 10,color:"#fff" }} onPress={() => navigation.dispatch(NavigationActions.back())} name="md-arrow-back" size={30} />
        )
      };
    }
  }
);

const ProjectsStack = createStackNavigator(
  {
    ProjectsExploreScreen: {screen:ProjectsExploreScreen},
    ProjectStack:{screen:ProjectStack,navigationOptions: ({ navigation }) => {
      return {
        headerShown:false,
        headerStyle:{backgroundColor:"#D04A02"},
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: 'bold',
        },
        title:" Nairobi County",
         headerLeft: (
          <Icon style={{ paddingLeft: 10,color:"#fff" }} onPress={() => navigation.openDrawer()} name="md-menu" size={30} />
        )
      };
    }},
  },
  {
    defaultNavigationOptions: ({ navigation }) => {
      return {
        headerStyle:{backgroundColor:"#D04A02"},
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: 'bold',
        },
        title:" Nairobi County",
         headerLeft: (
          <Icon style={{ paddingLeft: 10,color:"#fff" }} onPress={() => navigation.openDrawer()} name="md-menu" size={30} />
        )
      };
    }
  }
);


const DashboardStack = createStackNavigator(
  {
    DashboardScreen: {"screen":DashboardScreen,navigationOptions: ({ navigation }) => {
      
      return {
        headerStyle:{backgroundColor:"#D04A02"},
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: 'bold',
        },
        title:" Nairobi County",
         headerLeft: (
          <Icon style={{ paddingLeft: 10,color:"#fff" }} onPress={() => navigation.openDrawer()} name="md-menu" size={30} />
        )
      };
    }},
    ProjectStack: {
      screen: ProjectStack,
      navigationOptions:{
        header:null
      }
    },
  }
);




const AppDashboardDrawer = createDrawerNavigator({
  
  DashboardStack: {
    screen: DashboardStack
  },
 
  ProjectsStack: {
    screen: ProjectsStack
  },
  
  GalleryStack: {
    screen: GalleryStack
  },

  ProjectUsersStack: {
    screen: ProjectUsersStack
  }, 
  MyProfileStack: {
    screen: MyProfileStack
  },
  SettingsStack: {
    screen: SettingsStack
  },  
  NotificationsStack: {
    screen: NotificationsStack
  }, 
  AuthStack: {
    screen: AuthStack
  },
},
{  
    //initialRouteName: "Home",
    contentOptions: {
      activeTintColor: "#e91e63"
    },
    //hideStatusBar:true,
  //  drawerWidth:Dimensions.get("window").width*0.95,
    contentComponent: props => <SideBar {...props} />
  }
);

const RootSwitchNavigator = createSwitchNavigator({
  Auth : AppDashboardDrawer,
  AppDashboardDrawer: { screen: AppDashboardDrawer }
});





export default createAppContainer(RootSwitchNavigator);
