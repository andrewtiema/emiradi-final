import React from 'react';
import { View,ActivityIndicator,ScrollView,TextInput,Image,Dimensions } from 'react-native';
import ProjectList from '../components/ProjectList';
import { DataNavigation } from 'react-data-navigation';
import Config from './Config';

export default class DashboardScreen extends React.Component {
  
  constructor(props){
    super(props);
    this.state ={ isLoading: true}
  }


  componentDidMount(){
   
    DataNavigation.setData("project_fields",[
      "id",
      "project_photos",
      "project_manager",
      "featured_photo",
      "title",
      "description",
      "activities",
      "progress",
      "status"
    ]);
    return fetch(Config.get_project_api_route(),{   
      method: 'post', 
      body: JSON.stringify(
      {
          "fields":DataNavigation.getData("project_fields"),
      "static_fields":{
          "overall_status":"Project::get_overall_status",
          "ongoing_projects":"Project::get_ongoing_projects",
          "featured_project":"Project::get_featured_project",
          "completed_projects":"Project::get_completed_projects"
          }
      })}
      )
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          isLoading: false,
          projects: responseJson,
        }, function(){
         
          DataNavigation.setData('projects',this.state.projects.data);
        });

      })
      .catch((error) =>{
        
      });
  }

  render() {
    if(this.state.isLoading){
      return(
        <View style={{flex: 1, padding: 20}}>
          <ActivityIndicator/>
          
        </View>
      )

    }
    
    return (
     <ScrollView style={{ flex: 1}}> 
     
        

    


       <ProjectList projects={this.state.projects} navigation={this.props.navigation}/>      

       
       
       
        
      </ScrollView>
    );
  }
}