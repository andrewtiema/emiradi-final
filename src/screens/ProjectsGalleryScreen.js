import React, { Component } from 'react';
import {View,ActivityIndicator,Text} from 'react-native';
import { Container, Header, Tab,ScrollableTab, Left,Body,Right,Button,Title,Tabs, TabHeading, Icon } from 'native-base';
import ProjectGalleryScreen from './ProjectGalleryScreen';
import { DataNavigation } from 'react-data-navigation';
import {DataModel} from './DataModel';

export default class ProjectsGalleryScreen extends Component {
  constructor(props) {
    super(props);
    this.state ={ isLoading: true}
  }
  static navigationOptions = {
        headerStyle:{backgroundColor:"#D04A02"},
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: 'bold',textAlign: 'center',alignSelf:'center'
        },

        title: '80KM Road Tarmacking'
        
  };

  componentDidMount(){
    return fetch('https://portal.epesicloud.com/todo/default/graph/Project',{   
      method: 'post', 
      body: JSON.stringify(
        {
        "fields":[
            "project_photos",
            "project_manager",
            "title",
            "description",
            "activities"
        ],
        "activities":{
            "fields":["title","description","progress","progress"]
            }
        }
        )
      }
      
      )
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          isLoading: false,
          projects: responseJson,
        }, function(){
          DataNavigation.setData('projects',this.state.projects);
        });

      })
      .catch((error) =>{
        
      });
  }
  render() {
    if(this.state.isLoading){
      return(
        <View style={{flex: 1, padding: 20}}>
          <ActivityIndicator/>
        </View>
      )

    }
   let count=0;
    
    let projects_galleries = this.state.projects.map(function(project, key){
      count++;
      return (
        <Tab  heading={<TabHeading  style={{ backgroundColor: "#D04A02",color:"#fff" }}   >
          <Text style={{fontSize:10,color:"#fff"}}>{count}.{project.title}</Text></TabHeading>}>
          <ProjectGalleryScreen project={project} />
        </Tab>);
    }
    );
    return (
      <Container>
        


        
        <Tabs renderTabBar={()=> <ScrollableTab style={{ backgroundColor: "#D04A02" }} />}>

          {projects_galleries}
          
        </Tabs>
        
      </Container>
    );
  }
}