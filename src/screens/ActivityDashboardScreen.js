import React, { Component } from 'react';
import {Text,ScrollView} from 'react-native';
import { Container,Content, Header,Card,CardItem, Tab, Left,Body,Right,Button,Title,Tabs, TabHeading, Icon,Fab } from 'native-base';
import ActivitySummaryScreen from './ActivitySummaryScreen';
import ActivityUpdateScreen from './ActivityUpdateScreen';
import ActivityCommentsScreen from './ActivityCommentsScreen';
import ActivityAboutScreen from './ActivityAboutScreen';
import ActivityReportScreen from './ActivityReportScreen';
//import ActivityGalleryScreen from './ActivityGalleryScreen';


export default class ActivityDashboardScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: false
    };
  }
  
  static navigationOptions = {
        title: 'Activity : Road Surfacing',
        header: null,
  };
  render() {
    let activity=this.props.navigation.state.params.activity;
    return (
      <Container>
       

        <Tabs>
        

          <Tab    heading={ <TabHeading style={{ backgroundColor: "#E45C2B",color:"#FFF" }}   >
            <Icon name="pie" style={{color:"#fff"}} /><Text style={{fontSize:10,color:"#fff"}}> Summary</Text></TabHeading>}>
           
           <ScrollView>
            <ActivitySummaryScreen activity={activity} />
            
            <ActivityAboutScreen activity={activity} /> 
            </ScrollView>
            
          </Tab>
          <Tab  heading={ <TabHeading style={{ backgroundColor: "#E45C2B",color:"#FFF" }} ><Icon name="refresh" style={{color:"#fff"}} />
          <Text style={{fontSize:10,color:"#fff"}}  > Update History</Text></TabHeading>}>
          <ScrollView>
           <ActivityReportScreen activity={activity}/>
           </ScrollView>
            
          </Tab>
          <Tab  heading={ <TabHeading  style={{ backgroundColor: "#E45C2B",color:"#FFF" }} >
            <Icon name="chatbubbles" style={{color:"#fff"}} /><Text style={{fontSize:10,color:"#fff"}}> Conversation</Text></TabHeading>}>
            <ActivityCommentsScreen activity={activity} />
    </Tab>
        </Tabs>
        
        
        <Fab
            active={this.state.active}
            direction="up"
            style={{ backgroundColor: 'orange',width:80 }}
            position="bottomLeft"
            onPress={() => this.setState({ active: !this.state.active })}>
            <Text style={{flexDirection:"row",alignItems:"center",
            alignContent:"center",justifyContent:"center",fontSize:12,fontWeight:"bold"}}>
              <Icon style={{color:"#fff",fontSize:12}} name="ios-redo" /> PostReport </Text>
            <Button onPress={() => this.props.navigation.navigate('CameraScreen')}   style={{ backgroundColor: '#DD5144'}}>
              <Icon name="md-camera" />
            </Button>
            <Button onPress={() => this.props.navigation.navigate('ActivityUpdateScreen')}  style={{ backgroundColor: '#34A34F'}}>
              <Icon name="ios-create" />
            </Button>
          </Fab>
      </Container>
    );
  }
}