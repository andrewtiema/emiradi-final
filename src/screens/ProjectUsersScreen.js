import React, { Component } from 'react';
import {View,ActivityIndicator,Text} from 'react-native';
import {Container,Tab,Tabs,TabHeading,ScrollableTab} from 'native-base';
import UserList from '../components/UserList';
import {DataNavigation} from 'react-data-navigation';
export default class ProjectUsersScreen extends Component {

  constructor(props) {
    super(props);
    this.state ={ isLoading: true}
  }
  componentDidMount(){
    return fetch('https://portal.epesicloud.com/todo/default/graph/Project',{
      method: 'post',
      body: JSON.stringify(
      {
      "fields":[
          "users",
          "project_photos",
          "project_manager",
          "title",
          "description",
          "activities"
      ],
      "users":["name","profile_photo","position","activities","activities_completed","activities_ongoing","activities_stalled","activities_not_started"]
      })}

      )
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          isLoading: false,
          projects: responseJson,
        }, function(){
          DataNavigation.setData('projects',this.state.projects);
        });

      })
      .catch((error) =>{

      });
  }
  render() {
    if(this.state.isLoading){
      return(
        <View style={{flex: 1, padding: 20}}>
          <ActivityIndicator/>
        </View>
      )

    }

    let count=0;
    let navigation=this.props.navigation;
    let users_list = this.state.projects.map(function(project, key){
      count++;
      return (
        <Tab  heading={<TabHeading  style={{ backgroundColor: "#D04A02",color:"#fff" }}   >
          <Text style={{fontSize:10,color:"#fff"}}>{count}. {project.title}</Text></TabHeading>}>
          <UserList navigation={navigation} project={project} users={project.users} />
        </Tab>);
    }
    );

    return (
      <Container>

        <Tabs renderTabBar={()=> <ScrollableTab style={{ backgroundColor: "#D04A02" }} />}>

          {users_list}

        </Tabs>

      </Container>
    );
  }
}