import React from "react";
import { StyleSheet, Text, View, SafeAreaView, Image, ScrollView } from "react-native";
import { Ionicons, MaterialIcons } from "react-native-vector-icons";
import ProfileGalleryScreen from "./ProfileGalleryScreen";
import ProfileActivitiesScreen from "./ProfileActivitiesScreen";

export default class ProfileScreen extends React.Component {
    render()
    {
        const { navigation } = this.props;
        const user=navigation.getParam('user');
        return (
            <SafeAreaView style={styles.container}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    {/**<View style={styles.titleBar}>
                        <Ionicons name="ios-arrow-back" size={24} color="#52575D"></Ionicons>
                        <Ionicons name="md-more" size={24} color="#52575D"></Ionicons>
                    </View>**/}
    
                    <View style={{ alignSelf: "center",paddingTop:10 }}>
                        <View style={styles.profileImage}>
                            <Image source={{uri: 'http://todo.epesicloud.com/'+user.profile_photo}} style={styles.image} resizeMode="center"></Image>
                        </View>
                        <View style={styles.dm}>
                            <MaterialIcons name="chat" size={18} color="#DFD8C8"></MaterialIcons>
                        </View>
                        <View style={styles.active}></View>
                        {/**<View style={styles.add}>
                            <Ionicons name="ios-add" size={48} color="#DFD8C8" style={{ marginTop: 6, marginLeft: 2 }}></Ionicons>
                            </View>**/}
                    </View>
    
                    <View style={styles.infoContainer}>
                        <Text style={[styles.text, { fontWeight: "200", fontSize: 36 }]}>{user.first_name}</Text>
                        <Text style={[styles.text, { color: "#AEB5BC", fontSize: 14 }]}>{user.last_name}</Text>
                    </View>
    
                    <View style={styles.statsContainer}>
                        <View style={styles.statsBox}>
                            <Text style={[styles.text, { fontSize: 24 }]}>{user.activities?user.activities.length:0}</Text>
                            <Text style={[styles.text, styles.subText]}>Activities</Text>
                        </View>
                        <View style={[styles.statsBox, { borderColor: "#DFD8C8", borderLeftWidth: 1, borderRightWidth: 1 }]}>
                            <Text style={[styles.text, { fontSize: 24 }]}>{user.activities_ongoing?user.activities_ongoing.length:0}</Text>
                            <Text style={[styles.text, styles.subText]}>Ongoing</Text>
                        </View>
                        <View style={styles.statsBox}>
                            <Text style={[styles.text, { fontSize: 24 }]}>{user.activities_completed?user.activities_completed.length:0}</Text>
                            <Text style={[styles.text, styles.subText]}>Overdue</Text>
                        </View>
                    </View>
    
                    <View style={{ marginTop: 32 }}>
                        <ProfileGalleryScreen/>
                    </View>
                    <Text style={[styles.subText, styles.recent]}>Recent Activity</Text>
                    <View style={{ alignItems: "center" }}>
                        <ProfileActivitiesScreen/>
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#FFF"
    },
    text: {
        color: "#52575D"
    },
    image: {
        flex: 1,
        height: undefined,
        width: undefined
    },
    titleBar: {
        flexDirection: "row",
        justifyContent: "space-between",
        marginTop: 24,
        marginHorizontal: 16
    },
    subText: {
        fontSize: 12,
        color: "#AEB5BC",
        textTransform: "uppercase",
        fontWeight: "500"
    },
    profileImage: {
        width: 150,
        height: 150,
        borderRadius: 100,
        overflow: "hidden"
    },
    dm: {
        backgroundColor: "#41444B",
        position: "absolute",
        top: 20,
        width: 40,
        height: 40,
        borderRadius: 20,
        alignItems: "center",
        justifyContent: "center"
    },
    active: {
        backgroundColor: "#34FFB9",
        position: "absolute",
        bottom: 28,
        left: 10,
        padding: 4,
        height: 20,
        width: 20,
        borderRadius: 10
    },
    add: {
        backgroundColor: "#41444B",
        position: "absolute",
        bottom: 0,
        right: 0,
        width: 60,
        height: 60,
        borderRadius: 30,
        alignItems: "center",
        justifyContent: "center"
    },
    infoContainer: {
        alignSelf: "center",
        alignItems: "center",
        marginTop: 16
    },
    statsContainer: {
        flexDirection: "row",
        alignSelf: "center",
        marginTop: 32
    },
    statsBox: {
        alignItems: "center",
        flex: 1
    },
    mediaImageContainer: {
        width: 180,
        height: 200,
        borderRadius: 12,
        overflow: "hidden",
        marginHorizontal: 10
    },
    mediaCount: {
        backgroundColor: "#41444B",
        position: "absolute",
        top: "50%",
        marginTop: -50,
        marginLeft: 30,
        width: 100,
        height: 100,
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 12,
        shadowColor: "rgba(0, 0, 0, 0.38)",
        shadowOffset: { width: 0, height: 10 },
        shadowRadius: 20,
        shadowOpacity: 1
    },
    recent: {
        marginLeft: 78,
        marginTop: 32,
        marginBottom: 6,
        fontSize: 10
    },
    recentItem: {
        flexDirection: "row",
        alignItems: "flex-start",
        marginBottom: 16
    },
    activityIndicator: {
        backgroundColor: "#CABFAB",
        padding: 4,
        height: 12,
        width: 12,
        borderRadius: 6,
        marginTop: 3,
        marginRight: 20
    }
});
