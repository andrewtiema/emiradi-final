import React, { Component } from 'react';
import { Container, Header, Content, Button, ListItem, Text, Icon, Left, Body, Right, Switch } from 'native-base';
export default class SettingsScreen extends Component {
  render() {
    return (
      <Container>        
        <Content>
          <ListItem icon>
            <Left>
              <Button style={{ backgroundColor: "#FF9501" }}>
                <Icon active name="map" />
              </Button>
            </Left>
            <Body>
              <Text>Enable Maps</Text>
            </Body>
            <Right>
              <Text>Disabled</Text><Switch  />
            </Right>
          </ListItem>
          <ListItem icon>
            <Left>
              <Button style={{ backgroundColor: "#007AFF" }}>
                <Icon active name="list" />
              </Button>
            </Left>
            <Body>
              <Text>Activity Tracker </Text>
            </Body>
            <Right>
              <Text>Enabled</Text><Switch  />
            </Right>
          </ListItem>
          <ListItem icon>
            <Left>
              <Button style={{ backgroundColor: "#007AFF" }}>
                <Icon active name="settings" />
              </Button>
            </Left>
            <Body>
              <Text>App Version</Text>
            </Body>
            <Right>
              <Text>2.6</Text>
              <Icon active name="settings" />
            </Right>
          </ListItem>
        </Content>
      </Container>
    );
  }
}