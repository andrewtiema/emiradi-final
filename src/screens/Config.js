import React, { Component } from 'react';
export default class Config extends Component {

    static get_api_base()
    {
        //return "https://portal.epesicloud.com/postgre/graph/";
        return "https://portal.epesicloud.com/todo/default/graph/";
        //return "https://09e91afa.ngrok.io/postgre/graph/";
    }

    static get_project_api_route()
    {
        //return this.get_api_base()+"Project?account_id=122&consumer_key=23123hdfsdf";
        return this.get_api_base()+"Project";
    }
}