import React from 'react';
import { View,Text,Button,StyleSheet} from 'react-native';
import * as Progress from 'react-native-progress';
import { AnimatedCircularProgress } from 'react-native-circular-progress';

export default class ActivitySummaryScreen extends React.Component {
  
  render() {
    let progress_color=this.props.activity.overdue_days<0?"red":"#92d050";
    progress_color=this.props.activity.progress==0?"#febe03":progress_color;

    return (
       <View style={{flexDirection:"column",alignItems:"center",justifyContent:"center"}}>
         <Text style={{color:"silver",fontSize:20,marginTop:10}}>Road Surfacing</Text>
      <View style={{marginLeft:40}}>
      <AnimatedCircularProgress
  size={160}
  width={15}
  fill={this.props.activity.progress}
  prefill={100}
  tintColor={progress_color}
  onAnimationComplete={() => console.log('onAnimationComplete')}
  backgroundColor="#D3D3D3"
  dashedBackground={{width:10,gap:5}}
  rotation="270"
  arcSweepAngle="360">{fill => <Text style={styles.points}>{this.props.activity.progress}%</Text>}
 </AnimatedCircularProgress>
       
       </View>
       </View>
    );
  }
}

const styles = StyleSheet.create({
  points: {
    textAlign: 'center',
    color: '#7591af',
    fontSize: 35,
    fontWeight: '100',
  },
  container: {
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#152d44',
    padding: 50,
  },
  pointsDelta: {
    color: '#4c6479',
    fontSize: 50,
    fontWeight: '100',
  },
  pointsDeltaActive: {
    color: '#fff',
  },
});