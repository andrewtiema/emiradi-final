import React, { memo, useState,Component } from 'react';
import { AsyncStorage,TouchableOpacity, StyleSheet, Text, View,ActivityIndicator } from 'react-native';
import Background from '../components/Background';
import Logo from '../components/Logo';
import Header from '../components/Header';
import Button from '../components/Button';
import TextInput from '../components/TextInput';
import BackButton from '../components/BackButton';
import { theme } from '../core/theme';
import { emailValidator, passwordValidator } from '../core/utils';
import {DataNavigation} from 'react-data-navigation';

export default class ProjectDashboardScreen extends Component {
 
  constructor(props){
    super(props);
    this.state ={ 
      isLoading: false, 
      email:{
        value:"",
        error:""
      },
      password:{
        value:"",
        error:""
      }
    }
  } 

 

  login() {
    
    
  
    this.setState({isLoading: true});
    const navigation=this.props.navigation;
    //const emailError = emailValidator(this.state.email.value);
    let emailError=false;
    const passwordError = passwordValidator(this.state.password.value);

    if (emailError || passwordError) {
      this.state.email.error=emailError;
      this.state.password.error=passwordError;
      this.setState({isLoading: false});
      return;
    }
   


    return fetch('https://realty.scopicafrica.com/estate/api_v1/app_login',{
      method: 'post',
        body: JSON.stringify({
          "user_name":this.state.email.value,
          "password":this.state.password.value,
          "account_id":0
        })
      }).then((response) => {
      let response_json=response.json();
        
          return response_json;
        })
        .then((responseJson) => {
          console.log(responseJson);
          if(responseJson.info=="error")
          {
            this.setState({
              isLoading: false,
              password: {value:this.state.password,error:"Invalid Login Credentials"},
            });
          }
          else
          {
            this.setState({
              isLoading: false,
              user: responseJson.data,
            },function(){
              DataNavigation.setData('project',this.state.project);
            });
            AsyncStorage.setItem("first_name",this.state.user.first_name);
            AsyncStorage.setItem("last_name",this.state.user.last_name);
            AsyncStorage.setItem('user_id',this.state.user.id);
            AsyncStorage.setItem('profile_photo',this.state.user.profile_photo);
            navigation.navigate('AppDashboardDrawer');
            
          }
        })
        .catch((error) =>{
          console.error(error);
        });

        
       
       
    };
  

  render() {

    
  

    const { data, loading } = this.state;
    
    return (
      <Background>
        <BackButton goBack={() => navigation.navigate('HomeScreen')} />
  
        <Logo />
  
  
        <Header>Welcome back.</Header>
  

        <TextInput
          label="Email"
          returnKeyType="next"
          value={this.state.email.value}
          onChangeText={(text) => this.setState({
      email:{value:text,error:""}
    })}
          error={!!this.state.email.error}
          errorText={this.state.email.error}
          autoCapitalize="none"
          autoCompleteType="email"
          textContentType="emailAddress"
          keyboardType="email-address"
        />
  
        <TextInput
          label="Password"
          returnKeyType="done"
          value={this.state.password.value}
          onChangeText={text => this.setState({
            password:{value:text,error:""}
          })}
          error={!!this.state.password.error}
          errorText={this.state.password.error}
          secureTextEntry
        />
  
        <View style={styles.forgotPassword}>
          <TouchableOpacity
            onPress={() => navigation.navigate('ForgotPasswordScreen')}
          >
            <Text style={styles.label}>Forgot your password?</Text>
          </TouchableOpacity>
        </View>

        {this.state.isLoading ? <View style={{flex: 1, padding: 20}}><ActivityIndicator/></View> : <View><Text></Text></View>}
  
        <Button mode="contained" onPress={this.login.bind(this)}>
          Login
        </Button>
  
        
      </Background>
    );

  }

}


/**const LoginScreen = ({ navigation }) => {
  const [email, setEmail] = useState({ value: '', error: '' });
  const [password, setPassword] = useState({ value: '', error: '' });

  const _onLoginPressed = () => {
    const emailError = emailValidator(email.value);
    const passwordError = passwordValidator(password.value);

    if (emailError || passwordError) {
      setEmail({ ...email, error: emailError });
      setPassword({ ...password, error: passwordError });
      return;
    }
    navigation.navigate('AppDashboardDrawer');
    //navigation.navigate('Dashboard');
  };
  return (
    <Background>
      <BackButton goBack={() => navigation.navigate('HomeScreen')} />

      <Logo />


      <Header>Welcome back.</Header>

      <TextInput
        label="Email"
        returnKeyType="next"
        value={email.value}
        onChangeText={text => setEmail({ value: text, error: '' })}
        error={!!email.error}
        errorText={email.error}
        autoCapitalize="none"
        autoCompleteType="email"
        textContentType="emailAddress"
        keyboardType="email-address"
      />

      <TextInput
        label="Password"
        returnKeyType="done"
        value={password.value}
        onChangeText={text => setPassword({ value: text, error: '' })}
        error={!!password.error}
        errorText={password.error}
        secureTextEntry
      />

      <View style={styles.forgotPassword}>
        <TouchableOpacity
          onPress={() => navigation.navigate('ForgotPasswordScreen')}
        >
          <Text style={styles.label}>Forgot your password?</Text>
        </TouchableOpacity>
      </View>

      <Button mode="contained" onPress={_onLoginPressed}>
        Login
      </Button>

      
    </Background>
  );
};
export default memo(LoginScreen);
**/
const styles = StyleSheet.create({
  forgotPassword: {
    width: '100%',
    alignItems: 'flex-end',
    marginBottom: 24,
  },
  row: {
    flexDirection: 'row',
    marginTop: 4,
  },
  label: {
    //color: theme.colors.secondary,
    color: "#000",
    fontWeight:"700"
  },
  link: {
    fontWeight: 'bold',
    color: theme.colors.primary,
  },
});

