import React, { Component } from "react";
import { Dimensions, Platform,ImageBackground, View, StatusBar } from "react-native";
import { Container, Button, H3, Text,Input,Label,Item as FormItem,Form } from "native-base";
const deviceHeight = Dimensions.get("window").height;
var styles= {
  imageContainer: {
    flex: 1,
    width: null,
    height: null
  },
  logoContainer: {
    flex: 1,
    marginTop: deviceHeight / 8,
    marginBottom: 30
  },
  logo: {
    position: "absolute",
    left: Platform.OS === "android" ? 40 : 50,
    top: Platform.OS === "android" ? 35 : 60,
    width: 280,
    height: 100
  },
  text: {
    color: "#D8D8D8",
    bottom: 6,
    marginTop: 5
  }
};
const launchscreenBg = {uri:"https://realty.scopicafrica.com/assets/launchscreen-bg.png"};
const launchscreenLogo = {uri:"https://realty.scopicafrica.com/assets/logo-kitchen-sink.png"};

class SignInScreen extends Component {
  render() {
    return (
      <Container>
        <StatusBar barStyle="light-content" />
        <ImageBackground source={launchscreenBg} style={styles.imageContainer}>
          <View style={styles.logoContainer}>
            <ImageBackground source={launchscreenLogo} style={styles.logo} />
          </View>
          <View
            style={{
            padding:20
            }}
          >
            <Form>
              <FormItem floatingLabel>
                <Label>Email</Label>
                <Input />
              </FormItem>
              <FormItem floatingLabel last>
                <Label>Password</Label>
                <Input secureTextEntry={true} />
              </FormItem>

              <Button  primary style={{ paddingBottom: 4 }}>
                <Text> Login </Text>
              </Button>
              <Button  light primary><Text> Forgot Password</Text></Button>
            </Form>
          </View>
          <View style={{ marginBottom: 80 }}>
            
            <Button style={{ backgroundColor: "#6FAF98", alignSelf: "center" }} title="Login" onPress={() => this.props.navigation.navigate('Dashboard')}><Text>Login</Text></Button>
            <Button style={{ backgroundColor: "#6FAF98", alignSelf: "center" }} title="Forgot Password" onPress={() => this.props.navigation.navigate('ForgotPassword')}><Text>Forgot Password</Text></Button>
          </View>
        </ImageBackground>
      </Container>
    );
  }
}


export default SignInScreen;