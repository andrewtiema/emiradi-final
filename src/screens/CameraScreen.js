import React from 'react';
import { Camera } from 'expo-camera';
import { View, Text } from 'react-native';
import * as Permissions from 'expo-permissions';

import styles from '../styles';
import Toolbar from '../components/CameraToolbar';
import Gallery from '../components/CameraGallery';
let photos=[];
const createFormData = (uploads, body) => {
    const data = new FormData();
    
    if(Array.isArray(uploads))
    {
        photos=uploads;
    }
    else
    {
        photos[0]=uploads;
    }

       
    photos.map(function(photo,index){
        data.append('photo_'+index, {
            name: "Test",
            type: 'image/jpg',
            uri:
              Platform.OS === 'android' ? photo.uri : photo.uri.replace('file://', ''),
        });
    });
       
  
    Object.keys(body).forEach(key => {
      data.append(key, body[key]);
    });
    return data;
  };

export default class CameraScreen extends React.Component {
    camera = null;

    state = {
        captures: [],
        capturing: null,
        hasCameraPermission: null,
        cameraType: Camera.Constants.Type.back,
        flashMode: Camera.Constants.FlashMode.off,
    };

    setFlashMode = (flashMode) => this.setState({ flashMode });
    setCameraType = (cameraType) => this.setState({ cameraType });
    handleCaptureIn = () => this.setState({ capturing: true });

    handleCaptureOut = () => {
        if (this.state.capturing)
            this.camera.stopRecording();
    };

    handleShortCapture = async () => {
        const photoData = await this.camera.takePictureAsync();
        this.setState({ capturing: false, captures: [photoData, ...this.state.captures] });
        this.handleUploadPhoto();
    };

    handleLongCapture = async () => {
        const videoData = await this.camera.recordAsync();
        this.setState({ capturing: false, captures: [videoData, ...this.state.captures] });
    };

    handleUploadPhoto = () => {
       
        var photo = {
            uri: this.state.captures[0].uri,
            type: 'image/jpg',
            name: 'photo.jpg',
          };
          
          
          var form = new FormData();
          
          form.append("ProfilePicture", photo);


        fetch('https://webhook.site/3483fc6b-5140-4c1f-b53e-5cb5f6ce1c6b', {
          method: 'PUT',
          body: form,
          headers: {
            'Content-Type': 'multipart/form-data'
          }
        }).then(response => response.json()).catch((error) => {
            alert("ERROR : " + error);
          })
          .then(response => {
            this.setState({ photo: null });
          })
          .catch(error => {
            console.log('upload error', error);
            alert('Upload failed!');
          });
      };
    

    async componentDidMount() {
        const camera = await Permissions.askAsync(Permissions.CAMERA);
        const audio = await Permissions.askAsync(Permissions.AUDIO_RECORDING);
        const hasCameraPermission = (camera.status === 'granted' && audio.status === 'granted');

        this.setState({ hasCameraPermission });
    };

    render() {
        const { hasCameraPermission, flashMode, cameraType, capturing, captures } = this.state;

        if (hasCameraPermission === null) {
            return <View />;
        } else if (hasCameraPermission === false) {
            return <Text>Access to camera has been denied.</Text>;
        }

        return (
            <React.Fragment>
                <View>
                    <Camera
                        type={cameraType}
                        flashMode={flashMode}
                        style={styles.preview}
                        ref={camera => this.camera = camera}
                    />
                </View>

                {captures.length > 0 && <Gallery captures={captures}/>}

                <Toolbar 
                    capturing={capturing}
                    flashMode={flashMode}
                    cameraType={cameraType}
                    setFlashMode={this.setFlashMode}
                    setCameraType={this.setCameraType}
                    onCaptureIn={this.handleCaptureIn}
                    onCaptureOut={this.handleCaptureOut}
                    onLongCapture={this.handleLongCapture}
                    onShortCapture={this.handleShortCapture}
                />
            </React.Fragment>
        );
    };
};