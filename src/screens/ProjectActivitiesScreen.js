import React, { Component } from 'react';
import { Container, Header, Content, List, ListItem, Thumbnail, Text, Left, Body, Right, Button,Tabs,Tab,TabHeading,Icon } from 'native-base';
import {ProgressBarAndroid} from 'react-native';
import ActivityList from '../components/ActivityList';
import { DataNavigation } from 'react-data-navigation';
export default class ProjectActivitiesScreen extends Component {
  render() {
    
    return (
      <Container>   
        <Content>
        <Tabs>
          <Tab    heading={ <TabHeading style={{ backgroundColor: "#D04A01",color:"#fff" }}   ><Text style={{fontSize:10,color:"#fff"}}>All</Text></TabHeading>}>
             <ActivityList status="" activities={DataNavigation.getData('project').activities} navigation={this.props.navigation}/>
          </Tab>
          <Tab    heading={ <TabHeading style={{ backgroundColor: "#D04A01",color:"#fff" }}   ><Icon style={{fontSize:17,color:"#fff"}}  
          name="ios-repeat" /><Text style={{fontSize:9,color:"#fff"}}>In Progress</Text></TabHeading>}>
             <ActivityList status="Ongoing"  activities={DataNavigation.getData('project').activities_ongoing} navigation={this.props.navigation}/>
          </Tab>
          <Tab heading={ <TabHeading style={{ backgroundColor: "#D04A01",color:"#fff" }} ><Icon style={{fontSize:17,color:"#fff"}} 
           name="ios-checkmark" /><Text style={{fontSize:9,color:"#fff"}}>Complete</Text></TabHeading>}>
             <ActivityList status="Complete" activities={DataNavigation.getData('project').activities_completed} navigation={this.props.navigation}/>
            
          </Tab>
          <Tab   heading={ <TabHeading  style={{ backgroundColor: "#D04A01",color:"#fff" }} ><Icon style={{fontSize:17,color:"#fff"}} 
           name="ios-alert" /><Text style={{fontSize:9,color:"#fff"}} >Stalled</Text></TabHeading>}>
             <ActivityList status="Stalled" activities={DataNavigation.getData('project').activities_stalled} navigation={this.props.navigation}/>
          </Tab>
          <Tab  heading={ <TabHeading  style={{ backgroundColor: "#D04A01",color:"#fff" }} ><Icon style={{fontSize:17,color:"#fff"}}  
          name="ios-browsers" /><Text style={{fontSize:9,color:"#fff"}}>Not Started</Text></TabHeading>}>
            <ActivityList status="Not Started" activities={DataNavigation.getData('project').activities_not_started} navigation={this.props.navigation}/>
          </Tab>
        </Tabs>

        </Content>   
      </Container>
    );
  }
}