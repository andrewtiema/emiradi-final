import React, { Component } from 'react';
import {StyleSheet} from 'react-native';
import { Icon,Container, Header, Content, List, ListItem, Left, Body, Right, Thumbnail, Text } from 'native-base';

const styles=StyleSheet.create({
    notificationType:{
        color:"#ccc",
        textTransform:"uppercase",
        fontWeight:"bold"
    },
    notificationTypeIcon:{
        color:"#ccc",
        fontWeight:"bold",
        fontSize:15
    }
});
export default class NotificationsScreen extends Component {
  render() {
    return (
      <Container>
       
        <Content>
          <List>
            <ListItem avatar>
              <Left>
                <Icon styles={styles.notificationType} name="ios-mail"/>
              </Left>
              <Body>
                <Text styles={styles.notificationType}>INBOX</Text>
                <Text note>Hello sir, kindly review and approve completion of Project Nairobi</Text>
                <Text>Martin Muriithi</Text>
              </Body>
              <Right>
                <Text note>3:43 pm</Text>
              </Right>
            </ListItem>
            <ListItem avatar>
              <Left>
                <Icon styles={styles.notificationType} name="ios-speedometer"/>
              </Left>
              <Body>
                <Text>MONITORING</Text>
                <Text note>Nairobi Langata Road Phase B updated to 20% complete .</Text>
                <Text>Denis Kipchumba</Text>
              </Body>
              <Right>
                <Text note>3:43 pm</Text>
              </Right>
            </ListItem>
            <ListItem avatar>
              <Left>
                <Icon styles={styles.notificationType} name="ios-calendar"/>
              </Left>
              <Body>
                <Text>CALENDAR</Text>
                <Text note>Doing what you like will always keep you happy . .</Text>
                <Text>Langata Road Launch</Text>
              </Body>
              <Right>
                <Text note>3:43 pm</Text>
              </Right>
            </ListItem>
            <ListItem avatar>
              <Left>
                <Icon styles={styles.notificationType} name="ios-mail"/>
              </Left>
              <Body>
                <Text styles={styles.notificationType}>INBOX</Text>
                <Text note>Hello sir, kindly review and approve completion of Project Nairobi</Text>
                <Text>Martin Muriithi</Text>
              </Body>
              <Right>
                <Text note>3:43 pm</Text>
              </Right>
            </ListItem>
            <ListItem avatar>
              <Left>
                <Icon styles={styles.notificationType} name="ios-speedometer"/>
              </Left>
              <Body>
                <Text>MONITORING</Text>
                <Text note>Nairobi Langata Road Phase B updated to 20% complete .</Text>
                <Text>Denis Kipchumba</Text>
              </Body>
              <Right>
                <Text note>3:43 pm</Text>
              </Right>
            </ListItem>
            
          
          </List>
        </Content>
      </Container>
    );
  }
}
