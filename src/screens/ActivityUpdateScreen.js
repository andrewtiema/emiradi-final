import React, { Component } from 'react';
import {TextInput,Text,Keyboard,ActivityIndicator,ScrollView} from 'react-native';
import { View,Textarea,Container, Content, List, ListItem, InputGroup, Input, Icon, Picker, Button } from 'native-base';


export default class ActivityUpdateScreen extends Component {
 
  constructor(props)
  {
    super(props);
    this.state={
      kpi_result:'',
      description:'',
      status:'not_started',
      entities: [],
      isLoading: true

    };

    this.handleKpiResult = this.handleKpiResult.bind(this);
    this.handleDescription = this.handleDescription.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  post_entity(entity,data)
  {
    return fetch('https://portal.epesicloud.com/todo/default/save_entity/'+entity,{
      method: 'post',
      body: JSON.stringify(data)
    });
  }

  handleSubmit() {
    //we don't want the form to submit, so we prevent the default behavior
    var kpi_result = this.state.kpi_result;
    var description = this.state.description;
    
     if (!kpi_result || !description) {
      return;
    }
    this.post_entity("Activity_report",{
      kpi_result: kpi_result, 
      description: description,
      user_id:122,
      project_id:26,
      user_id:423,
      user_id:122,
      component_id:65
    });
    alert("Successfully Submitted Record");
    this.setState({
      kpi_result: '',
      description: ""
    });
  }

  handleDescription(description) {
    this.setState({ description });
  }

  handleKpiResult(kpi_result) {
    this.setState({ kpi_result });
  }

  fetch_statuses()
  {
    return fetch('https://portal.epesicloud.com/todo/default/graph/Status',{
    method: 'post',
    body: JSON.stringify({})
  }).then((response) => {
        return response.json(); 
      })
      .then((responseJson) => {
        var statuses=responseJson;
        
        this.setState({
          isLoading: false,
          statuses: statuses,
        }, function(){
          
        });

      })
      .catch((error) =>{
        console.error(error);
      });
  }

  componentDidMount(){
    this.fetch_statuses();
    //Fetch Activity Details
  }
  

 
  

  render() {
    
    if(this.state.isLoading){
      return(
        <View style={{flex: 1, padding: 20}}>
          <ActivityIndicator/>
        </View>
      )
    }
    const status_list=this.state.statuses.map(function(status,index){
      return <Picker.Item label={status.title} value={status.id} />
    });
    return (
      <ScrollView>
      <View style={{padding:20,flex:1,alignContent:"center"}}>
              
              <View style={{marginVertical:10}}>
                  <Text>
                      Report Date : 12th February 2020
                  </Text>
              </View>
             


              <View style={{marginVertical:10}}>
                  <Text style={{fontWeight:"bold"}}>Target Unit :  </Text>
                  <Text style={{color:"#ccc",fontWeight:"bold"}}> Number of Classrooms built </Text>
              </View>

              <View style={{marginVertical:10}}>
                  <Text style={{fontWeight:"bold"}}>Target Unit :  </Text>
                  <Text style={{color:"#ccc",fontWeight:"bold"}}> 1,000 </Text>
              </View>

              <View style={{marginVertical:10}}>
              <Text> Current Status (Whats the current status of the activity?) </Text>
              <Picker
              mode="dropdown"
              selectedValue={this.state.status}
              style={{height: 50, width: 100}}
              onValueChange={(itemValue, itemIndex) =>
                this.setState({status: itemValue})
              }
                  style={{height: 50, width: 200, borderColor: 'gray', borderWidth: 1}}>
                 {status_list}
                  </Picker>
              </View>

             

              <View style={{marginVertical:10}}>
              <Text style={{marginVertical:10}}> Target Archieved (Input the target units met so far)</Text>

              <TextInput
                  placeholder="Input Target Achived"
                  maxLength={20}
                  onBlur={Keyboard.dismiss}
                  value={this.state.kpi_result}
                  onChangeText={this.handleKpiResult}
                />
              </View>


              <View style={{marginVertical:10}}>
              <Text style={{marginVertical:10}}> Comments (Any additional notes) </Text>
              <TextInput
                  style={{ padding:10,height: 80, borderColor: 'gray', borderWidth: 1 }}
                  multiline
                  numberOfLines={4}
                  editable
                  placeholder="Additional Comments"
                  maxLength={20}
                  onBlur={Keyboard.dismiss}
                  value={this.state.description}
                  onChangeText={this.handleDescription}
                  
              />
              </View>


              

              <Button  
                      onPress={this.handleSubmit.bind(this)} 
                      style={{flex:1,justifyContent:"center",alignContent:"center",alignSelf:"center",width:200,backgroundColor:"#D04A02",padding:10}} mode="contained">
                          <Text style={{color:"#fff",fontWeight:"bold",textAlign:"right"}}>POST UPDATE</Text></Button>
          </View>
          </ScrollView>
  );
  }
}