import React, { Component } from 'react';
import { View,Container, Header, Content, List, ListItem, Thumbnail, Text, Left, Body, Right, Button } from 'native-base';
import { DataNavigation } from 'react-data-navigation';
export default class ProjectAboutScreen extends Component {
  render() {
    return (
      <View>
          <List>
            <ListItem thumbnail>
              <Body>
                <Text>Sector {DataNavigation.getData('name')}</Text>
                <Text note numberOfLines={2}>Transport & Development</Text>
              </Body>
            </ListItem>
            <ListItem thumbnail>
              <Body>
                <Text>Project</Text>
                <Text note numberOfLines={2}>{this.props.project.about.title}</Text>
              </Body>
            </ListItem>
            <ListItem thumbnail>
              <Body>
                <Text>Project Description</Text>
                <Text note numberOfLines={2}>{this.props.project.about.description}</Text>
              </Body>
              
            </ListItem>
            <ListItem thumbnail>
              <Body>
                <Text>Objectives</Text>
                <Text note numberOfLines={2}>{this.props.project.about.objectives}</Text>
              </Body>
              
            </ListItem>
            <ListItem thumbnail>
              <Body>
                <Text>Due Date</Text>
                <Text note numberOfLines={2}>2020-12-01</Text>
              </Body>
            </ListItem>

            

            <ListItem thumbnail>
              <Body>
                <Text>Project Manager</Text>
                <Text note numberOfLines={2}>{this.props.project.project_manager.first_name} {this.props.project.project_manager.last_name}</Text>
              </Body>
            </ListItem>
            
            
            
            <ListItem thumbnail>
              <Body>
                <Text>Last Update</Text>
                <Text note numberOfLines={2}>2020-09-12</Text>
              </Body>
            </ListItem>
            
          </List>
        </View>
    );
  }
}