import React from 'react';
import {ActivityIndicator} from 'react-native';
import { GiftedChat } from 'react-native-gifted-chat';
export default class UserChat extends React.Component {
  state = {
    messages: [],
  }

  push_message(message)
  {
    fetch('https://portal.epesicloud.com/todo/default/save_entity/User_comment',{
      method: 'post',
      body: JSON.stringify({
        "content":message,
        "project_id":26,
        "user_id":423
      })
    }).then((response) => {
      let response_json=response.json();        
      return response_json;
    }).then((responseJson) => {
      console.log(responseJson);
    });
  }

  refetch_messages()
  {
    return fetch('https://portal.epesicloud.com/todo/default/graph/User_chat',{
    method: 'post',
    body: JSON.stringify({
      "id":26
    })
  }).then((response) => {
        let response_json=response.json();        
        return response_json;
      })
      .then((responseJson) => {
        var chat_comments=responseJson.user_comments;
        var messages=chat_comments.map(function(message,index){
            return {
              _id: message.id,
              text: message.content,
              createdAt: message.created_at,
              user: {
                _id: message.user.id,
                name: message.user.first_name+" "+message.user.last_name,
                avatar: 'https://ha.epesicloud.com/'+message.user.profile_photo,
              },
            };
            
        });

        this.setState({
          isLoading: false,
          messages: messages,
        }, function(){
          
        });

      })
      .catch((error) =>{
        console.error(error);
      });
  }
  
  componentDidMount(){
    this.refetch_messages();
  }
  
  onSend(messages = []) {
    this.push_message(messages[0].text);
    this.setState(previousState => ({
      messages: GiftedChat.append(previousState.messages, messages),
    }))
  }

  render() {
    
    if(this.state.isLoading){
      return(
        <View style={{flex: 1, padding: 20}}>
          <ActivityIndicator/>
        </View>
      )
    }

    return (
      <GiftedChat
        messages={this.state.messages}
        onSend={messages => this.onSend(messages)}
        user={{
          _id: 1,
        }}
      />
    )
  }
}