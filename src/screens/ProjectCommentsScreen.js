import React from 'react';
import {ActivityIndicator,View,AsyncStorage} from 'react-native';
import { GiftedChat } from 'react-native-gifted-chat';
export default class ProjectCommentsScreen extends React.Component {
  state = {
    messages: [],
    isLoading:true,
    user_id:null
  }

  retrieve_user = async () => {
    try {
      const user_id = await AsyncStorage.getItem('user_id');
      if (value !== null) {
       this.setState({"user_id":user_id});
      }
    } catch (error) {
      // Error retrieving data
    }
  };

  push_message(message)
  {
    console.log(this.state.user_id);
    fetch('http://todo.epesicloud.com/todo/default/save_entity/Project_comment?account_id=1&consumer_key=56rtt',{
      method: 'post',
      body: JSON.stringify({
        "content":message,
        "project_id":this.props.project.id,
        "user_id":this.state.user_id
      })
    }).then((response) => {
      let response_json=response.json();        
      return response_json;
    }).then((responseJson) => {
      console.log(responseJson);
    });
  }

  refetch_messages()
  {
    return fetch('https://portal.epesicloud.com/todo/default/graph/Project?account_id=1&consumer_key=56rtt',{
    method: 'post',
    body: JSON.stringify({
      "id":this.props.project.id,
      "fields":[
        "project_comments",
      ],
      "project_comments":{
        "fields":["id","content","title","created_at","user"]
      }
    })
  }).then((response) => {
        let response_json=response.json();        
        return response_json;
      })
      .then((responseJson) => {
        var project_comments=responseJson.project_comments;
        var messages=[];
       if(project_comments==undefined)
       {
        
       }
       else
       {
        messages=project_comments.map(function(message,index){
          return {
               _id: message.id,
               text: message.content,
               createdAt: message.created_at,
               user: {
                 _id: message.user.id,
                 name: message.user.first_name+" "+message.user.last_name,
                 avatar: 'http://todo.epesicloud.com/'+message.user.profile_photo,
               },
             };
             
         });
       }
       
      messages.reverse();

        this.setState({
          isLoading: false,
          messages: messages,
        }, function(){
          
        });

      })
      .catch((error) =>{
        console.error(error);
      });
  }
  
  componentDidMount(){
    this.setState({
      isLoading: true
    });
    this.refetch_messages();
  }
  
  onSend(messages = []) {
    this.push_message(messages[0].text);
    this.setState(previousState => ({
      messages: GiftedChat.append(previousState.messages, messages),
    }))
  }

  render() {
    
    if(this.state.isLoading){
      return(
        <View style={{flex: 1, padding: 20}}>
          <ActivityIndicator/>
        </View>
      )
    }

    return (
      <GiftedChat
        messages={this.state.messages}
        onSend={messages => this.onSend(messages)}
        user={{
          _id: AsyncStorage.getItem("user_id"),
        }}
      />
    )
  }
}