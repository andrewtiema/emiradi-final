import React, { Component } from 'react';
import { StyleSheet, Text, View,ActivityIndicator} from 'react-native';
import { WebView } from 'react-native-webview';
import { DataNavigation } from 'react-data-navigation';
// ...
export default class ProjectMapScreen extends Component {
  constructor(props) {
    super(props);
    this.state = { visible: true };
  }
 
  showSpinner() {
    console.log('Show Spinner');
    this.setState({ visible: true });
  }
 
  hideSpinner() {
    console.log('Hide Spinner');
    this.setState({ visible: false });
  }

  render() {
    
    return (
      <View style={{flex:1}}>
        
        

          {this.state.visible ? (
            <ActivityIndicator
              color="#009688"
              size="large"
              style={styles.ActivityIndicatorStyle}
            />
          ) : null}

       
        <WebView style={styles.WebViewStyle}
        //Loading URL
        source={{ uri: 'https://ha.epesicloud.com/todo/default/component/Project/google_map/'+DataNavigation.getData('project').id }}
        //Enable Javascript support
        javaScriptEnabled={true}
        //For the Cache
        domStorageEnabled={true}
        //View to show while loading the webpage
        //Want to show the view or not
        //startInLoadingState={true}
        onLoadStart={() => this.showSpinner()}
        onLoad={() => this.hideSpinner()}
        />
         
        
       </View>
    );
  }
}

const styles = StyleSheet.create({
  styleOld: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  styleNew: {
    flex: 1,
  },
  WebViewStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    marginTop: 0,
    backgroundColor:"#f0f0f0"
  },
  ActivityIndicatorStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    //position: 'absolute',
  },
});