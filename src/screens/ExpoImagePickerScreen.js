import React, { Component } from 'react';
import {
  ActivityIndicator,
  Clipboard,
  Image,
  Share,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,Keyboard,TextInput
} from 'react-native';
import {Icon,Button } from 'native-base';
import * as Permissions from 'expo-permissions';
import * as ImagePicker from 'expo-image-picker';

export default class App extends Component {
  

  constructor(props)
  {
    super(props);
    this.state={
        image: null,
        uploading: false,
        description:null
      };
    this.handleDescription = this.handleDescription.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleDescription(description) {
    this.setState({ description });
  }


  post_entity(entity,data)
  {
    return fetch('http://todo.epesicloud.com/todo/default/save_entity/'+entity+'?account_id=122&consumer_key=544',{
      method: 'post',
      body: JSON.stringify(data)
    }).then(response => response.json())
    .then(responseJson => {
     
      this.setState(
        {
          isLoading: false,
          dataSource: responseJson,
        },
        function() {
          
         
        }
      );
    })
    .catch(error => {
      console.error(error);
    });
  }

  handleSubmit() {
    //we don't want the form to submit, so we prevent the default behavior
    var description = this.state.description;
    var image=this.state.image;
     if (!image || !description) {
      return;
    }
    this.post_entity("Activity_file",{
      file_link: image, 
      description: description,
      user_id:122,
      project_id:26,
      activity_id:2
      
    });
    alert("Successfully Submitted Record");
    this.setState({
      image: '',
      description: ""
    });
  }

  render() {
    let {
      image
    } = this.state;

    return (
      <View style={styles.container}>
        <StatusBar barStyle="default" />

        <Text
          style={styles.exampleText}>
          Take photos or pick a photo from your library.
        </Text>

        <View style={{ flexDirection: "row", flex: 1, justifyContent: 'space-around', padding: 10 }}>
          
          <Button iconLeft style={styles.button}   onPress={this._takePhoto}>
            <Icon name="camera" />
            <Text style={{color:"#fff"}}> Take photo</Text>           
          </Button>

          <Button style={styles.button} iconLeft onPress={this._pickImage}>
            <Icon name="image" />
            <Text style={{color:"#fff"}}> Pick Image</Text>
          </Button>


        </View>


       

        {this._maybeRenderImage()}
        {this._maybeRenderUploadingOverlay()}
      </View>
    );
  }

  _maybeRenderUploadingOverlay = () => {
    if (this.state.uploading) {
      return (
        <View
          style={[StyleSheet.absoluteFill, styles.maybeRenderUploading]}>
          <ActivityIndicator color="#fff" size="large" />
        </View>
      );
    }
  };

  _maybeRenderImage = () => {
    let {
      image
    } = this.state;

    if (!image) {
      return;
    }

    return (
      <View
        style={styles.maybeRenderContainer}>
        <View
          style={styles.maybeRenderImageContainer}>
          <Image source={{ uri: image }} style={styles.maybeRenderImage} />
        </View>

        <TextInput
                  style={{ margin:10,padding:10,borderRadius:10, borderColor: 'gray', borderWidth: 1 }}
                  multiline
                  numberOfLines={1}
                  editable
                  placeholder="Additional Description"
                  maxLength={20}
                  onBlur={Keyboard.dismiss}
                  value={this.state.description}
                  onChangeText={this.handleDescription}
                  
              />

                    <Button  
                   
                      onPress={this.handleSubmit.bind(this)} 
                      style={{justifyContent:"center",color:"#fff",alignContent:"center",alignSelf:"center",width:200,backgroundColor:"#D04A02",padding:10}}
                       mode="contained"><Text style={{color:"#fff"}}>Post Update</Text></Button>

         
        
        <Text
          onPress={this._copyToClipboard}
          onLongPress={this._share}
          style={/**styles.maybeRenderImageText**/{display:"none"}}>
          {image}
        </Text>

      </View>
    );
  };

  _share = () => {
    Share.share({
      message: this.state.image,
      title: 'Check out this photo',
      url: this.state.image,
    });
  };

  _copyToClipboard = () => {
    Clipboard.setString(this.state.image);
    alert('Copied image URL to clipboard');
  };

  _takePhoto = async () => {
    const {
      status: cameraPerm
    } = await Permissions.askAsync(Permissions.CAMERA);

    const {
      status: cameraRollPerm
    } = await Permissions.askAsync(Permissions.CAMERA_ROLL);

    // only if user allows permission to camera AND camera roll
    if (cameraPerm === 'granted' && cameraRollPerm === 'granted') {
      let pickerResult = await ImagePicker.launchCameraAsync({
        allowsEditing: false,
        aspect: [4, 3],
      });

      this._handleImagePicked(pickerResult);
    }
  };

  _pickImage = async () => {
    const {
      status: cameraRollPerm
    } = await Permissions.askAsync(Permissions.CAMERA_ROLL);

    // only if user allows permission to camera roll
    if (cameraRollPerm === 'granted') {
      let pickerResult = await ImagePicker.launchImageLibraryAsync({
        allowsEditing: true,
        aspect: [4, 3],
      });

      this._handleImagePicked(pickerResult);
    }
  };

  _handleImagePicked = async pickerResult => {
    let uploadResponse, uploadResult;

    try {
      this.setState({
        uploading: true
      });

      if (!pickerResult.cancelled) {
        uploadResponse = await uploadImageAsync(pickerResult.uri);
        uploadResult = await uploadResponse.json();

        this.setState({
          image: uploadResult.location
        });
      }
    } catch (e) {
      console.log({ uploadResponse });
      console.log({ uploadResult });
      console.log({ e });
      alert('Upload failed, sorry :(');
    } finally {
      this.setState({
        uploading: false
      });
    }
  };
}

async function uploadImageAsync(uri) {
  let apiUrl = 'https://file-upload-example-backend-dkhqoilqqn.now.sh/upload';

  // Note:
  // Uncomment this if you want to experiment with local server
  //
  // if (Constants.isDevice) {
  //   apiUrl = `https://your-ngrok-subdomain.ngrok.io/upload`;
  // } else {
  //   apiUrl = `http://localhost:3000/upload`
  // }

  let uriParts = uri.split('.');
  let fileType = uriParts[uriParts.length - 1];

  let formData = new FormData();
  formData.append('photo', {
    uri,
    name: `photo.${fileType}`,
    type: `image/${fileType}`,
  });

  let options = {
    method: 'POST',
    body: formData,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'multipart/form-data',
    },
  };

  return fetch(apiUrl, options);
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flex: 1
  },
  exampleText: {
    fontSize: 20,
    marginBottom: 20,
    marginHorizontal: 15,
    textAlign: 'center',
    color:"#A9A9A9"
  },
  maybeRenderUploading: {
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.4)',
    justifyContent: 'center',
  },
  maybeRenderContainer: {
    borderRadius: 3,
    elevation: 2,
    marginTop: -20,
    padding:20,
    shadowColor: 'rgba(0,0,0,1)',
    shadowOpacity: 0.2,
    shadowOffset: {
      height: 4,
      width: 4,
    },
    shadowRadius: 5,
    width: 250,
  },
  maybeRenderImageContainer: {
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3,
    overflow: 'hidden',
  },
  maybeRenderImage: {
    height: 250,
    width: 250,
  },
  maybeRenderImageText: {
    paddingHorizontal: 10,
    paddingVertical: 10,
  },
  button: {
    paddingHorizontal: 10,
    marginLeft:20,
    backgroundColor:"#D04A01"
  },
  title_text:{
    fontWeight:"bold"
  },
  button_text:{
    padding:10,
    color:"#fff"
  }
});