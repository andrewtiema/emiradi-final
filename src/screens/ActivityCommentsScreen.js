import React from 'react';
import {ActivityIndicator,View,AsyncStorage} from 'react-native';
import { GiftedChat } from 'react-native-gifted-chat';
export default class ProjectCommentsScreen extends React.Component {
  state = {
    messages: [],
    isLoading:true
  }

  push_message(message)
  {
    
    fetch('http://todo.epesicloud.com/todo/default/save_entity/Activity_comment?account_id='+AsyncStorage.getItem('account_id')+'&consumer_key=56rtt',{
      method: 'post',
      body: JSON.stringify({
        "content":message,
        "activity_id":this.props.activity.id,
        "user_id":AsyncStorage.getItem("user_id")
      })
    }).then((response) => {
      let response_json=response.json();        
      return response_json;
    }).then((responseJson) => {
      //console.log(responseJson);
    });
  }

  refetch_messages()
  {
    return fetch('https://portal.epesicloud.com/todo/default/graph/Activity?account_id='+AsyncStorage.getItem('account_id')+'&consumer_key=56rtt',{
    method: 'post',
    body: JSON.stringify({
      "id":this.props.activity.id,
      "fields":[
        "activity_comments",
      ],
      "activity_comments":{
        "fields":["id","content","title","created_at","user"]
      }
    })
  }).then((response) => {
        let response_json=response.json();        
        return response_json;
      })
      .then((responseJson) => {
        var activity_comments=responseJson.activity_comments;
        var messages=[];
       if(activity_comments==undefined)
       {
        
       }
       else
       {
        messages=activity_comments.map(function(message,index){
          return {
               _id: message.id,
               text: message.content,
               createdAt: message.created_at,
               user: {
                 _id: message.user.id,
                 name: message.user.first_name+" "+message.user.last_name,
                 avatar: 'http://todo.epesicloud.com/'+message.user.profile_photo,
               },
             };
             
         });
       }
       
      messages.reverse();

        this.setState({
          isLoading: false,
          messages: messages,
        }, function(){
          
        });

      })
      .catch((error) =>{
        console.error(error);
      });
  }
  
  componentDidMount(){
    this.setState({
      isLoading: true
    });
    this.refetch_messages();
  }
  
  onSend(messages = []) {
    this.push_message(messages[0].text);
    this.setState(previousState => ({
      messages: GiftedChat.append(previousState.messages, messages),
    }))
  }

  render() {
    
    if(this.state.isLoading){
      return(
        <View style={{flex: 1, padding: 20}}>
          <ActivityIndicator/>
        </View>
      )
    }

    return (
      <GiftedChat
        messages={this.state.messages}
        onSend={messages => this.onSend(messages)}
        user={{
          _id: AsyncStorage.getItem("user_id"),
        }}
      />
    )
  }
}