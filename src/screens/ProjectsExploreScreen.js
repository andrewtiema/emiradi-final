import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,
    SafeAreaView,
    TextInput,
    Platform,
    StatusBar,
    ScrollView,
    Image,
    Dimensions,
    Animated,
    ActivityIndicator
} from "react-native";
import Icon from 'react-native-vector-icons/Ionicons'
import Category from '../components/Explore/Category'
import Home from '../components/Explore/Home'
import Tag from '../components/Explore/Tag'
import { TouchableOpacity } from "react-native-gesture-handler";
import {DataNavigation} from 'react-data-navigation';
const { height, width } = Dimensions.get('window')
class ProjectsExploreScreen extends Component {

    constructor(props) {
        super(props);
        //setting default state
        this.state = { isLoading: true};
      }

    componentDidMount(){
        return fetch('https://portal.epesicloud.com/todo/default/graph/Project',{   
            method: 'post', 
            body: JSON.stringify(
            {
            "static_fields":{
                "overall_status":"Project::get_overall_status",
                "ongoing_projects":"Project::get_ongoing_projects",
                "featured_project":"Project::get_featured_project",
                "completed_projects":"Project::get_completed_projects"
                }
            })}
            )
          .then((response) => response.json())
          .then((responseJson) => {
              console.log(responseJson);
            this.setState({
              isLoading: false,
              ongoing_projects: responseJson.ongoing_projects,
              featured_project: responseJson.featured_project,
              completed_projects: responseJson.completed_projects
            }, function(){
              
            });
    
          })
          .catch((error) =>{
            
          });
      }
    componentWillMount() {

        this.scrollY = new Animated.Value(0)

        this.startHeaderHeight = 80
        this.endHeaderHeight = 50
        if (Platform.OS == 'android') {
            this.startHeaderHeight = 100 + StatusBar.currentHeight
            this.endHeaderHeight = 70 + StatusBar.currentHeight
        }

        this.animatedHeaderHeight = this.scrollY.interpolate({
            inputRange: [0, 50],
            outputRange: [this.startHeaderHeight, this.endHeaderHeight],
            extrapolate: 'clamp'
        })

        this.animatedOpacity = this.animatedHeaderHeight.interpolate({
            inputRange: [this.endHeaderHeight, this.startHeaderHeight],
            outputRange: [0, 1],
            extrapolate: 'clamp'
        })
        this.animatedTagTop = this.animatedHeaderHeight.interpolate({
            inputRange: [this.endHeaderHeight, this.startHeaderHeight],
            outputRange: [-30, 10],
            extrapolate: 'clamp'
        })
        this.animatedMarginTop = this.animatedHeaderHeight.interpolate({
            inputRange: [this.endHeaderHeight, this.startHeaderHeight],
            outputRange: [50, 30],
            extrapolate: 'clamp'
        })


    }

    render() {
        if(this.state.isLoading){
          return(
            <View style={{flex: 1, padding: 20}}>
              <ActivityIndicator/>
            </View>
          )
    
        }

        let ongoing_projects_list=this.state.ongoing_projects.map(function(project,index){
            return  <TouchableOpacity onPress={() => this.props.navigation.navigate('ProjectStack')}>
            <Category imageUri={{uri:"https://ha.epesicloud.com/"+project.featured_photo}}
                name={project.title}
            />
            </TouchableOpacity>;
        });

        let completed_projects_list=this.state.completed_projects.map(function(project,index){
            
            return <TouchableOpacity onPress={() => this.props.navigation.navigate('ProjectStack')}>
                <Home width={width}
                    featured_photo={"https://ha.epesicloud.com/"+project.featured_photo}
                    name={project.title}
                    type="Education"
                    progress={project.progress}
                />
            </TouchableOpacity>
        });

        let featured_project=this.state.featured_project;

        let featured_project_view=<TouchableOpacity onPress={() => this.props.navigation.navigate('ProjectStack')}>
        <Text style={{ fontSize: 24, fontWeight: '700' }}>
            Featured Project : {featured_project.title}
        </Text>
        <Text style={{ fontWeight: '100', marginTop: 10 }}>
            {featured_project.description}

        </Text>
        <View style={{ width: width - 40, height: 200, marginTop: 20 }}>
            <Image 
                style={{ flex: 1, height: null, width: null, resizeMode: 'cover', borderRadius: 5, borderWidth: 1, borderColor: '#dddddd' }}
                source={{uri:"https:/ha.epesicloud.com/"+featured_project.featured_photo}}
            />
            

        </View>
    </TouchableOpacity>;
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={{ flex: 1 }}>
                   {/** <Animated.View style={{ height: this.animatedHeaderHeight, backgroundColor: 'white', borderBottomWidth: 1, borderBottomColor: '#dddddd' }}>
                        <View style={{
                            flexDirection: 'row', padding: 10,
                            backgroundColor: 'white', marginHorizontal: 20,
                            shadowOffset: { width: 0, height: 0 },
                            shadowColor: 'black',
                            shadowOpacity: 0.2,
                            elevation: 1,
                            marginTop: Platform.OS == 'android' ? 30 : null
                        }}>
                            <Icon name="ios-search" size={20} style={{ marginRight: 10 }} />
                            <TextInput
                                underlineColorAndroid="transparent"
                                placeholder="Find a project..."
                                placeholderTextColor="grey"
                                style={{ flex: 1, fontWeight: '700', backgroundColor: 'white' }}
                            />
                        </View>
                        <Animated.View
                            style={{ flexDirection: 'row', marginHorizontal: 20, position: 'relative', top: this.animatedTagTop, opacity: this.animatedOpacity }}
                        >
                            <Tag name="Roads" />
                            <Tag name="Schools" />

                        </Animated.View>
                    </Animated.View>**/}
                    <ScrollView
                        scrollEventThrottle={16}
                        onScroll={Animated.event(
                            [
                                { nativeEvent: { contentOffset: { y: this.scrollY } } }
                            ]
                        )}
                    >
                        <View style={{ flex: 1, backgroundColor: 'white', paddingTop: 20 }}>
                            <Text style={{ fontSize: 24, fontWeight: '700', paddingHorizontal: 20 }}>
                                Ongoing projects ({this.state.ongoing_projects.length})
                            </Text>

                            <View style={{ height: 130, marginTop: 20 }}>
                                <ScrollView
                                    horizontal={true}
                                    showsHorizontalScrollIndicator={false}
                                >
                                    
                                    {ongoing_projects_list}
                                    
                                </ScrollView>
                            </View>
                            <View style={{ marginTop: 40, paddingHorizontal: 20 }}>
                                {featured_project_view}
                            </View>
                        </View>
                        <View style={{ marginTop: 40 }}>
                            <Text style={{ fontSize: 24, fontWeight: '700', paddingHorizontal: 20 }}>
                              Completed Projects ({this.state.completed_projects.length})
                            </Text>
                            <View style={{ paddingHorizontal: 20, marginTop: 20, flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'space-between' }}>
                                {completed_projects_list}


                            </View>
                        </View>
                    </ScrollView>

                </View>
            </SafeAreaView>
        );
    }
}
export default ProjectsExploreScreen;