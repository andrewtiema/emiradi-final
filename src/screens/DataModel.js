import React, { Component } from 'react';
import { DataNavigation } from 'react-data-navigation';
export default class DataModel extends Component {

    static dateToYMD(date) {
        var strArray=['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        var d = date.getDate();
        var m = strArray[date.getMonth()];
        var y = date.getFullYear();
        return '' + (d <= 9 ? '0' + d : d) + '-' + m + '-' + y;
    }
    static fetch_project_fields()
    {
        return [
            "id",
            "project_photos",
            "project_manager",
            "title",
            "description",
            "activities",
            "stages",
            "updates",
            "health",
            "progress",
            "status",
            "about",
            "project_locations",
            "project_comments"
          ];
    }
    
    static fetch_project()
    {

    }

    static fetch_active_project()
    {
        return DataNavigation.getData('project');
    }

    static fetch_projects_users()
    {
        return fetch('https://portal.epesicloud.com/todo/default/graph/Project',{   
            method: 'post', 
            body: JSON.stringify(
            {
                "static_fields":{
                        "projects_users":"Project::get_all_users"
                }
            })}
        );
    }

    static fetch_static_overall_data()
    {
        return fetch('https://portal.epesicloud.com/todo/default/graph/Project',{   
            method: 'post', 
            body: JSON.stringify(
            {
            "static_fields":{
                "overall_status":"Project::get_overall_status",
                "ongoing_projects":"Project::get_ongoing_projects",
                "featured_project":"Project::get_featured_project",
                "completed_projects":"Project::get_completed_projects"
                }
            })}
            );
    }
    

    static fetch_all_projects_data()
    {
        return fetch('https://portal.epesicloud.com/todo/default/graph/Project',{   
            method: 'post', 
            body: JSON.stringify(
            {
                "fields":DataModel.fetch_project_fields(),
                "activities":{
                    "fields":["title","description","progress","progress"]
                    },
            "static_fields":{
                "overall_status":"Project::get_overall_status",
                "ongoing_projects":"Project::get_ongoing_projects",
                "featured_project":"Project::get_featured_project",
                "completed_projects":"Project::get_completed_projects"
                }
            })}
            );
    }

    


    static fetch_project_list()
    {
        return fetch('https://portal.epesicloud.com/todo/default/graph/Project',{   
            method: 'post', 
            body: JSON.stringify(
            {
            "fields":[
                "project_photos",
                "project_manager",
                "title",
                "description",
                "activities",
                "activities_ongoing",
                "activities_stalled",
                "activities_not_started",
                "activities_completed",
                "stages",
                "updates",
                "health",
                "progress",
                "status",
                "about",
                "project_locations",
                "comments"
            ],
            "activities":{
                "fields":["title","description","progress","progress"]
                }
            })}
            
            );
    }
}