import React, { Component } from 'react';
import {View,ActivityIndicator} from 'react-native';
import { Container, Header, Tab, Left,Body,Right,Button,Title,Tabs, TabHeading, Icon, Text } from 'native-base';
import ProjectSummaryScreen from './ProjectSummaryScreen';

import ProjectCommentsScreen from './ProjectCommentsScreen';
import ProjectGalleryScreen from './ProjectGalleryScreen';
import ProjectAboutScreen from './ProjectAboutScreen';
import { ScrollView } from 'react-native-gesture-handler';
import { DataNavigation } from 'react-data-navigation';
import Config from './Config';
export default class ProjectDashboardScreen extends Component {
 
  constructor(props){
    super(props);
    this.state ={ isLoading: true,project:this.props.navigation.getParam("project")}
  }  
  

  componentDidMount(){
    
    
    return fetch(Config.get_project_api_route(),{
    method: 'post',
    body: JSON.stringify({
      "id":this.state.project.id,
      "fields":[
        "id",
        "project_manager",
        "project_photos",
        "title",
        "description",
        "activities",
        "activities_stalled",
        "activities_not_started",
        "activities_ongoing",
        "activities_completed",
        "progress",
        "status",
        "about",
        "project_locations",
        "project_comments"
      ],
      "activities":{
        "fields":["title","description","progress","start_date","end_date","component",
        "overdue_days","is_overdue","team","user","project"]
      }
    })
  }).then((response) => {
        let response_json=response.json();       

        return response_json;
      })
      .then((responseJson) => {
        
        this.setState({
          isLoading: false,
          project: responseJson,
        }, function(){
          DataNavigation.setData('project',this.state.project);
        });

      })
      .catch((error) =>{
        console.error(error);
      });
  }
 
  render() {
    
    if(this.state.isLoading){
      return(
        <View style={{flex: 1, padding: 20}}>
          <ActivityIndicator/>
        </View>
      )
    }

    return (
      <Container>
        
        
        <Tabs>

          <Tab  heading={<TabHeading  style={{ backgroundColor: "#D04A02",color:"#fff" }}><Icon name="stats" style={{color:"#fff"}} /><Text style={{fontSize:10,color:"#fff"}}>Summary</Text></TabHeading>}>
            <ProjectSummaryScreen navigation={this.props.navigation} project={this.state.project} />
          </Tab>

          <Tab  heading={ <TabHeading style={{ backgroundColor: "#D04A02",color:"#fff" }}  ><Icon name="camera" style={{color:"#fff"}} /><Text style={{fontSize:10,color:"#fff"}}>Gallery</Text></TabHeading>}>
            <ProjectGalleryScreen project={this.state.project} />
          </Tab>
          
          <Tab  heading={ <TabHeading  style={{ backgroundColor: "#D04A01",color:"#fff" }} ><Icon name="chatbubbles" style={{color:"#fff"}} /><Text style={{fontSize:10,color:"#fff"}} tabStyle={{ backgroundColor: "#D04A01" }} >Updates</Text></TabHeading>}>
            <ProjectCommentsScreen project={this.state.project} />
          </Tab>
          <Tab  heading={ <TabHeading style={{ backgroundColor: "#D04A01",color:"#fff" }} ><Icon name="list" style={{color:"#fff"}} /><Text style={{fontSize:10,color:"#fff"}} tabStyle={{ backgroundColor: "#D04A01" }} >About</Text></TabHeading>}>
            <ScrollView>
              <ProjectAboutScreen project={this.state.project}/>
            </ScrollView>
            
          </Tab>
          
        </Tabs>
        
      </Container>
    );
  }
}