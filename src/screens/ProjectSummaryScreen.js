import React from 'react';
import { View,Text,Button,ScrollView} from 'react-native';
import ProjectPerformancePie from '../components/ProjectPerformancePie';
import ActivitiesProgressBar from '../components/ActivitiesProgressBar';

export default class ProjectSummaryScreen extends React.Component {
  
  render() {
   
    return (
      <ScrollView style={{ flex: 1,marginTop:10}}>     
     
        <ProjectPerformancePie status={this.props.project.status} alignSelf="center" />


      
         <View style={{paddingHorizontal:20,paddingVertical:10,fontSize:14}}>
          <Text style={{fontWeight:"bold"}}> Ongoing Activities ({this.props.project.activities.length}) </Text>
          <ActivitiesProgressBar navigation={this.props.navigation} activities={this.props.project.activities}/>
        </View>

      
      
      </ScrollView>
    );
  }
}