import React, { Component } from 'react';
import {View,Text,ActivityIndicator,FlatList} from 'react-native';
import EvidenceList from "../components/EvidenceList";
import ActivityUpdateForm from "../components/ActivityUpdateForm";
import { Card,Container,Accordion, Header, Content,  CardItem, Body,List,ListItem } from 'native-base';
const dataArray = [
  { title: "Report Date : 2019-12-13, Target Archived : 20 km done", content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. " },
];

function ActivityReport({entity }) {
  
  const dataArray = [
    { title: "Target Archived :"+entity.kpi_result, content: entity.description },
  ];
  return (
    <Card>
          <Text style={{padding:10}}>Report Date : {entity.report_date}</Text>
          <Accordion
            dataArray={dataArray}
            icon="add"
            expandedIcon="remove"
            iconStyle={{ color: "green" }}
            expandedIconStyle={{ color: "red" }}
          />
          <View>
            <EvidenceList/>
          </View>
          <Text style={{padding:10}}>{entity.user.first_name+" "+entity.user.last_name}</Text>
       </Card>
  );
}

export default class ActivityReportScreen extends React.Component {
 

  constructor(props)
  {
    super(props);
    this.state={
      entities: [],
      isLoading: true,
    };
  }

  

  fetch_entities()
  {
    return fetch('https://portal.epesicloud.com/todo/default/graph/Activity_report',{
    method: 'post',
    body: JSON.stringify({
      "fields":["id","title","description","report_date","kpi_metric","kpi_result","user"]
    })
  }).then((response) => {
        return response.json(); 
      })
      .then((responseJson) => {
        var entities=responseJson;
        
        this.setState({
          isLoading: false,
          entities: entities,
        }, function(){
          
        });

      })
      .catch((error) =>{
        console.error(error);
      });
  }

  ListViewItemSeparator = () => {
    //Item sparator view
    return (
      <View
        style={{
          height: 0.3,
          width: '90%',
          backgroundColor: '#080808',
        }}
      />
    );
  };
  
  componentDidMount(){
    this.fetch_entities();

  }
  
  

  render() {
    
    if(this.state.isLoading){
      return(
        <View style={{flex: 1, padding: 20}}>
          <ActivityIndicator/>
        </View>
      )
    }
    const entities=this.state.entities;
    return (

      <FlatList
          /**ListHeaderComponent={this.renderHeader}**/
          data={entities}
          ItemSeparatorComponent={this.ListViewItemSeparator}
          //Item Separator View
          renderItem={({ item }) => (
            // Single Comes here which will be repeatative for the FlatListItems
            <ActivityReport id={item.id}            
            title={item.title}
            entity={item}/>

          )}
          enableEmptySections={true}
          style={{ marginTop: 10 }}
          keyExtractor={(item, index) => index.toString()}
        />
    );
  }
}