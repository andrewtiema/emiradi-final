import React, { Component } from 'react';
import { Image,Text,StyleSheet } from 'react-native';

import { Container, View, DeckSwiper, Card, CardItem, Thumbnail, Left, Body, Icon,Button } from 'native-base';

export default class ProjectGalleryScreen extends Component {
  constructor(props) {
    super(props);
    this.state={
      'project':this.props.project
    };
  }
  
  render() {
    let project=this.state.project;    
    return (
      <Container>
       
        <View>
          <DeckSwiper
            ref={(c) => this._deckSwiper = c}
            dataSource={project.project_photos}
            renderEmpty={() =>
              <View style={{ alignSelf: "center" }}>
                <Text>Over</Text>
              </View>
            }
            renderItem={photo =>
              <Card style={{ elevation: 3 }}>
                <CardItem>
                  <Left>
                    <Thumbnail source={{uri:"https://portal.epesicloud.com/"+photo.image_link}} />
                    <Body>
                      <Text style={styles.title_text}>{project.title}</Text>
            <Text note>{project.description}</Text>
                    </Body>
                  </Left>
                </CardItem>
                <CardItem cardBody>
                  <Image style={{ height: 250, flex: 1 }} source={{uri:"https://portal.epesicloud.com/"+photo.image_link}} />
                </CardItem>
                <CardItem>
                  <Text>{photo.description}</Text>
                  <Text> ({project.activities.length} activities)</Text>
                </CardItem>
              </Card>
            }
          />
        </View>
        <View style={{ flexDirection: "row", flex: 1, position: "absolute", bottom: 20, left: 0, right: 0,justifyContent: 'space-between', padding: 10 }}>
          <Button style={styles.button} iconLeft onPress={() => this._deckSwiper._root.swipeLeft()}>
            <Icon name="arrow-back" />
            <Text  style={styles.button_text}>Swipe Left</Text>
          </Button>
          <Button style={styles.button} iconRight onPress={() => this._deckSwiper._root.swipeRight()}>
            
            <Text style={styles.button_text}>Swipe Right</Text>
            <Icon name="arrow-forward" />
          </Button>
        </View>
      </Container>
      
    );
  }
}


var styles = StyleSheet.create({
  button: {
      paddingHorizontal: 10,
      
      backgroundColor:"#D04A01"
    },
    title_text:{
      fontWeight:"bold"
    },
    button_text:{
      padding:10,
      color:"#fff"
    }
  });