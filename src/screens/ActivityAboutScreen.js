import React, { Component } from 'react';
import {View,Text} from "react-native";
import { Container, Header, Content, List, ListItem, Thumbnail, Left, Body, Right, Button } from 'native-base';
export default class ActivityAboutScreen extends Component {
  render() {
    let activity=this.props.activity;
    return (
      <View>
      <List>
            
            
            <ListItem thumbnail>
              <Body>
                <Text>Activity Name</Text>
                <Text note numberOfLines={2}>{activity.title}</Text>
              </Body>
              
            </ListItem>
            <ListItem thumbnail>
              <Body>
                <Text>Activity Description</Text>
                <Text note numberOfLines={2}>{activity.description}</Text>
              </Body>
              
            </ListItem>
            <ListItem thumbnail>
              <Body>
                <Text>Project</Text>
                <Text note numberOfLines={2}>{activity.project.title}</Text>
              </Body>
            </ListItem>
            <ListItem thumbnail>
              <Body>
                <Text>Start Date</Text>
                <Text note numberOfLines={2}>{activity.start_date}</Text>
              </Body>
            </ListItem>
            <ListItem thumbnail>
              <Body>
                <Text>Due Date</Text>
                <Text note numberOfLines={2}>{activity.end_date}</Text>
              </Body>
            </ListItem>

            

            <ListItem thumbnail>
              <Body>
                <Text>Assigned To</Text>
                <Text note numberOfLines={2}>{activity.user.first_name} {activity.user.last_name}</Text>
              </Body>
            </ListItem>
            
            <ListItem thumbnail>
              <Body>
                <Text>Target Unit</Text>
                <Text note numberOfLines={2}>{activity.target_unit}</Text>
              </Body>
            </ListItem>
            <ListItem thumbnail>
              <Body>
                <Text>Evidence Required</Text>
                <Text note numberOfLines={2}>Photos of project progress</Text>
              </Body>
            </ListItem>
            <ListItem thumbnail>
              <Body>
                <Text>Target Achieved</Text>
                <Text note numberOfLines={2}>{activity.progress}% Archieved</Text>
              </Body>
            </ListItem>
            <ListItem thumbnail>
              <Body>
                <Text>Last Update</Text>
                <Text note numberOfLines={2}>2020-09-12</Text>
              </Body>
            </ListItem>            
          </List>
          </View>
    );
  }
}