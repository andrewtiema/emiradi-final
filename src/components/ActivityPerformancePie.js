import React from "react";
import { Dimensions,StyleSheet, View,Text,ScrollView } from "react-native";
import {
  LineChart,
  BarChart,
  PieChart,
  ProgressChart,
  ContributionGraph,
  StackedBarChart
} from "react-native-chart-kit";
const sample_data=[
   { x: "Complete", y: 35 },
  { x: " ", y: 40 },
   
    
  ];
  const chartConfig = {
  backgroundGradientFrom: "#fff",
  backgroundGradientFromOpacity: 0,
  backgroundGradientTo: "#fff",
  backgroundGradientToOpacity: 0.5,
  color: (opacity = 1) => `rgb(36, 152, 123, ${opacity})`,
  strokeWidth: 2, // optional, default 3
  barPercentage: 0.5
};
const screenWidth = Dimensions.get("window").width;

const data = {
  labels: ["Pending","Complete"], // optional
  data: [0.8, 0.8]
};

export default class App extends React.Component {
  render() {
    return (
     
            <ProgressChart
        data={data}
        width={screenWidth}
        height={220}
        chartConfig={chartConfig}
        hideLegend={true}
      />
    );
  }
}