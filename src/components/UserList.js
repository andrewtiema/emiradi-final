import React, { Component } from 'react';
import { List, ListItem, Thumbnail, Left, Body, Right, Button } from 'native-base';
import {Text} from 'react-native';
export default class UserList extends Component {
  render() {
    let count=0;
   
   const navigation=this.props.navigation;
    if(this.props.users)
    {
      const project=this.props.project;
      let users_list = this.props.users.map(function(user, index){
        console.log(user.activities);
        count++;
        return (
            <ListItem key={index} avatar>
            <Left>
              <Thumbnail source={{ uri: "http://todo.epesicloud.com/"+user.profile_photo }} />
            </Left>
            <Body>
              <Text style={{fontWeight:"bold"}}>{user.name}</Text>
              <Text note>{user.position}</Text>
              <Text style={{color:"#ccc"}}>{user.role=="project_manager"?user.project:user.activity}</Text>
              <Text style={{color:"#ccc"}}>({user.activities?user.activities.length:0} activities assigned) </Text>
              <Text style={{color:"#ccc"}}>Last Active : </Text>
            </Body>
            <Right>                
              <Button onPress={()=>navigation.navigate('ProfileScreen',{user:user})} 
              transparent style={{padding:5}}><Text style={{color:"#000",fontWeight:"bold"}}>View Profile</Text></Button>
            </Right>
          </ListItem>);
      }
      );
      return (
        <List>
          {users_list}
        </List>
      );
    }
    else
    {
      return (
        <List>
          <ListItem thumbnail>
          <Left>
            
          </Left>
          <Body>
            <Text>(0) Users </Text>
            <Text note numberOfLines={2}> There are currently no users</Text>
            
          </Body>
          <Right>
            
          </Right>
        </ListItem>
        </List>
      );
    }
    
  }
}