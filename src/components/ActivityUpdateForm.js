import React, { Component } from 'react';
import {TextInput,Text} from 'react-native';
import { View,Textarea,Container, Content, List, ListItem, InputGroup, Input, Icon, Picker, Button } from 'native-base';


export default class ActivityUpdateForm extends Component {

    constructor(props)
    {
        super(props);
        this.state={
            status:'not_started'
        };
        
    }
    
    render() {
        return (
            <View style={{padding:20,flex:1,alignContent:"center"}}>
                    
                    <View style={{marginVertical:10}}>
                        <Text>
                            Report Date : 12th February 2020
                        </Text>
                    </View>
                   


                    <View style={{marginVertical:10}}>
                        <Text style={{fontWeight:"bold"}}>Target Unit :  </Text>
                        <Text style={{color:"#ccc",fontWeight:"bold"}}> Number of Classrooms built </Text>
                    </View>

                    <View style={{marginVertical:10}}>
                        <Text style={{fontWeight:"bold"}}>Target Unit :  </Text>
                        <Text style={{color:"#ccc",fontWeight:"bold"}}> 1,000 </Text>
                    </View>

                    <View style={{marginVertical:10}}>
                    <Text> Current Status (Whats the current status of the activity?) </Text>
                    <Picker
                    mode="dropdown"
                    selectedValue={this.state.status}
                    style={{height: 50, width: 100}}
                    onValueChange={(itemValue, itemIndex) =>
                      this.setState({status: itemValue})
                    }
                        style={{height: 50, width: 200, borderColor: 'gray', borderWidth: 1}}>
                        <Picker.Item label="Not Started" value="not_started" />
                        <Picker.Item label="In Progress" value="in_progress" />
                        <Picker.Item label="Complete" value="complete" />
                        <Picker.Item label="Stalled" value="stalled" />
                        </Picker>
                    </View>

                   

                    <View style={{marginVertical:10}}>
                    <Text style={{marginVertical:10}}> Target Archieved (Input the target units met so far)</Text>

                    <TextInput
                        style={{ padding:10,height: 40, borderColor: 'gray', borderWidth: 1 }}
                        label="Email"
                        placeholder="Target Achieved"
                        autoCapitalize="none"
                    />
                    </View>


                    <View style={{marginVertical:10}}>
                    <Text style={{marginVertical:10}}> Comments (Any additional notes) </Text>
                    <TextInput
                        placeholder="Comments"
                        style={{ padding:10,height: 80, borderColor: 'gray', borderWidth: 1 }}
                        multiline
                        numberOfLines={4}
                        editable
                    />
                    </View>


                    

                    <Button  
                            onPress={() => alert('Activity Updated')} 
                            style={{flex:1,justifyContent:"center",alignContent:"center",alignSelf:"center",width:200,backgroundColor:"#D04A02",padding:10}} mode="contained">
                                <Text style={{color:"#fff",fontWeight:"bold",textAlign:"right"}}>POST UPDATE</Text></Button>
                </View>
        );
    }
}