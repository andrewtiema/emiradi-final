import React,{ Component } from 'react';
import { TouchableOpacity,Text,Image,View,StyleSheet} from 'react-native';
import { Container, Header, Content, Card, CardItem, Thumbnail, Button, Icon, Left, Body, Right } from 'native-base';
import * as Progress from 'react-native-progress';
import { Divider } from 'react-native-elements';
import ActivityProgressBar from './ActivityProgressBar';
import { DataNavigation } from 'react-data-navigation'; 
import {NavigationActions} from 'react-navigation';
export default class ActivitiesProgressBar extends Component {
 

  render() {
    let activity_list=[];
    let activities=this.props.activities;
    let activity_count=0;
    
    const navigation=this.props.navigation;
    activities.map(function(activity,index){
      activity_count++;
      activity_list.push(
        <View key={index}>
          
           <ActivityProgressBar navigation={navigation} activity={activity} activity_count={activity_count}/>
          
          <Divider style={{ backgroundColor: '#ccc',marginTop:10 }} />
        </View>
      );
    });
    return (      
        

        <View>              
        {activity_list}
              
       </View>
   
    );
  }
}

const styles = StyleSheet.create({
  
  smallText: {
    fontSize: 12,
    fontWeight: 'bold',
  },
});