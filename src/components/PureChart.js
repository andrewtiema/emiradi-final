/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import PureChart from 'react-native-pure-chart';

export default class PureChartTest extends Component {
  render(){
    let sampleData = [
      {
        value: 50,
        label: 'Marketing',
        color: 'red',
      }, {
        value: 40,
        label: 'Sales',
        color: 'blue'
      }, {
        value: 25,
        label: 'Support',
        color: 'green'
      }
  
    ]
    return <PureChart  data={sampleData} type='pie' />;

  }
  
}