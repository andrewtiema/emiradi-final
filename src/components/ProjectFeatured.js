import React,{ Component } from 'react';
import { Image,View,StyleSheet,ProgressBarAndroid} from 'react-native';
import { Container, Header, Content, Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body, Right } from 'native-base';
export default class ProjectFeatured extends Component {
  render() {
    return (
      <View style={{paddingLeft:10,paddingRight:10}}>
       <Card>
            <CardItem>
              <Left>
                <Thumbnail source={{uri: 'https://constructionreviewonline.com/wp-content/uploads/2019/03/road.jpg'}} />
                <Body>
                  <Text>Sample Project</Text>
                  <Text note>Martin Muriithi(Owner)</Text>
                </Body>
              </Left>
            </CardItem>
            <CardItem cardBody>
              <Image source={{uri: 'https://constructionreviewonline.com/wp-content/uploads/2019/03/road.jpg'}} style={{height: 200, width: null, flex: 1}}/>
            </CardItem>
            <CardItem>
              <Left>
                <Button transparent>
                  
                  <Text style={styles.smallText}>12% Complete</Text>
                  <ProgressBarAndroid
                    styleAttr="Horizontal"
                    indeterminate={false}
                    progress={0.5}
                  />
                </Button>
              </Left>
              <Body>
                 
                
              </Body>
              <Right>
                <Button transparent   onPress={() => this.props.navigation.navigate('ProjectStack')}>
                  <Icon active name="eye" />
                  <Text style={styles.smallText} >View</Text>
                </Button>
              </Right>
            </CardItem>
          </Card>
    
    
    </View>
    );
  }
}

const styles = StyleSheet.create({
  
  smallText: {
    fontSize: 12,
    fontWeight: 'bold',
  },
});