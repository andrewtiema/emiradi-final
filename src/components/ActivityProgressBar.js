import React,{ Component } from 'react';
import { TouchableOpacity,Text,Image,View,StyleSheet} from 'react-native';
import { Container, Header, Content, Card, CardItem, Thumbnail, Button, Icon, Left, Body, Right } from 'native-base';
import * as Progress from 'react-native-progress';
import { Divider } from 'react-native-elements';
import {DataModel} from '../screens/DataModel';
import { DataNavigation } from 'react-data-navigation'; 
import {NavigationActions} from 'react-navigation';
export default class ActivityProgressBar extends Component {
  render() {
    let activity=this.props.activity;
    function dateToYMD(date) {
      var strArray=['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
      var d = date.getDate();
      var m = strArray[date.getMonth()];
      var y = date.getFullYear();
      return '' + (d <= 9 ? '0' + d : d) + '-' + m + '-' + y;
  } 
  let progress_color=activity.overdue_days<0?"red":"#92d050";
   progress_color=activity.progress==0?"#febe03":progress_color;
   
   let activity_progress=activity.progress?activity.progress:0;
   let activity_progress_percentage=activity_progress?activity_progress/100:0;
   const navigation=this.props.navigation;
    return (      
      <TouchableOpacity
      onPress={function(){
        DataNavigation.setData("activity",activity);                  
        navigation.navigate('ActivityDashboardStack', {}, 
        NavigationActions.navigate({ routeName: 'ActivityDashboardScreen',params:{activity:activity} }))
      }}>
        <View style={{paddingHorizontal:20,paddingVertical:10}}>
              <View style={{flexDirection:"row",justifyContent:"space-between"}}>


                <View>
                  <Text style={styles.smallText}>{this.props.activity_count}. {activity.title}</Text>
                </View>

                <View>
                    <Text style={{color:"#ccc"}}> Due : {dateToYMD(new Date(activity.end_date))}</Text>
                </View>
                

              </View>

             <View>
                  <Text style={{width:200}}>{activity.description}</Text>
            </View>
                
              
              
              <View style={{flexDirection:"row",justifyContent:"space-between"}} >

                <View style={{marginTop:10}}>
                <Progress.Bar progress={activity_progress_percentage} color={progress_color} width={180} />
                </View>

                <View>
                    <Text style={{color:"#ccc"}}> {activity_progress}% progress</Text>
                </View>    
                
                
              </View>

             
              
            </View>
        </TouchableOpacity>
   
    );
  }
}

const styles = StyleSheet.create({
  
  smallText: {
    fontSize: 12,
    fontWeight: 'bold',
  },
});