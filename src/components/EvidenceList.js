import React, { Component } from 'react';
import { Container, Header, Content, List, ListItem, Thumbnail, Left, Body, Right, Button } from 'native-base';
import {ProgressBarAndroid,Text} from 'react-native';
export default class EvidenceList extends Component {
  render() {
    return (
      <List>
            <ListItem thumbnail>
              <Left>
                <Thumbnail square source={{ uri: 'https://constructionreviewonline.com/wp-content/uploads/2019/06/Road_construction_in_Bayelsa_State.jpg' }} />
              </Left>
              <Body>
                <Text>Evidence : </Text>
                <Text note numberOfLines={2}>Photo for the progress.</Text>
                <ProgressBarAndroid
          styleAttr="Horizontal"
          indeterminate={false}
          progress={0.5}
        /> 
              </Body>
              <Right>
                
              </Right>
            </ListItem>
            

          </List>
    );
  }
}