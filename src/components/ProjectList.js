/*This is an Example of SearchBar in React Native*/
import * as React from 'react';
import {TouchableOpacity, Text, View, StyleSheet, FlatList, ActivityIndicator, Platform } from 'react-native';
import { SearchBar } from 'react-native-elements';
import Constants from 'expo-constants';
import ProjectCard from "./ProjectCard";
import { DataNavigation } from 'react-data-navigation';
import {NavigationActions} from 'react-navigation';
import OverallPerformancePie from './OverallPerformancePie';
import Config from '../screens/Config';

function Item({ id,project, title, selected, onSelect,navigation }) {
  
    return (
      <TouchableOpacity
      onPress={function(){
        DataNavigation.setData("project",project);
        DataNavigation.setData("activities_count",project.activities.length);                  
        navigation.navigate('ProjectDashboardTab', {}, 
        NavigationActions.navigate({ routeName: 'ProjectDashboardScreen',params:{project:project} }))
      }}
      
       // onPress={() => onSelect(id)}
       /**style={[
          { backgroundColor: selected ? '#6e3b6e' : '#f9c2ff' },
        ]}**/
      >
        <ProjectCard project={project} navigation={navigation}/>
       
      </TouchableOpacity>
    );
  }

export default class App extends React.Component {

  constructor(props) {
    super(props);
    //setting default state
    this.state = { isLoading: true, search: '' };
    this.arrayholder = [];
  }
  componentDidMount() {
    return fetch(Config.get_project_api_route(),{   
        method: 'post', 
      body: JSON.stringify(
        {          
        "fields":DataNavigation.getData("project_fields"),
        "activities":{
          "fields":["title","description","progress"]
          }
        })}
      )
      .then(response => response.json())
      .then(responseJson => {
        this.setState(
          {
            isLoading: false,
            dataSource: responseJson,
          },
          function() {
            this.arrayholder = responseJson;
            DataNavigation.setData('projects',this.state.projects);
          }
        );
      })
      .catch(error => {
        console.error(error);
      });
  }
  
  search = text => {
    console.log(text);
  };
  clear = () => {
    this.search.clear();
  };
  SearchFilterFunction(text) {
    //passing the inserted text in textinput
    const newData = this.arrayholder.filter(function(item) {
      //applying filter for the inserted text in search bar
      const itemData = item.title ? item.title.toUpperCase() : ''.toUpperCase();
      //const itemData = `${item.title.toUpperCase()} ${item.description.toUpperCase()}`;
      
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });
    this.setState({
      //setting the filtered newData on datasource
      //After setting the data it will automatically re-render the view
      dataSource: newData,
      search:text,
    });
  }
  ListViewItemSeparator = () => {
    //Item sparator view
    return (
      <View
        style={{
          height: 0.3,
          width: '90%',
          backgroundColor: '#080808',
        }}
      />
    );
  };
  renderHeader = () => {
    return (
      <SearchBar
        placeholder="Type Here..."
        //lightTheme
        //round
        style={{marginTop:10,marginLeft:14,marginRight:14}}
        searchIcon={{ size: 24 }}
        onChangeText={text => this.SearchFilterFunction(text)}
        //onClear={text => this.SearchFilterFunction('')}
        //autoCorrect={false}
        value={this.state.search}
      />
    );
  };

  render() {
    if (this.state.isLoading) {
      //Loading View while data is loading
      return (
        <View style={{ flex: 1, paddingTop: 20 }}>
          <ActivityIndicator />
        </View>
      );
    }

    return (
      //ListView to show with textinput used as search bar

        
      <View>
        <SearchBar
                lightTheme
                round
                searchIcon={{ size: 24 }}
                onChangeText={text => this.SearchFilterFunction(text)}
                onClear={text => this.SearchFilterFunction('')}
                placeholder="Type Here..."
                value={this.state.search}
                />

        <View style={{flexDirection:"column",alignItems:"center",justifyCotnent:"center",backgroundColor:"#fff",marginTop:5,marginLeft:14,marginRight:14,height:350}}>          
          <Text>Overall Performance</Text>
          
          <OverallPerformancePie status={this.props.projects.static.overall_status}/>
          
          </View>


          <View style={styles.viewStyle}>


      

          <FlatList
          /**ListHeaderComponent={this.renderHeader}**/
          data={this.state.dataSource}
          ItemSeparatorComponent={this.ListViewItemSeparator}
          //Item Separator View
          renderItem={({ item }) => (
            // Single Comes here which will be repeatative for the FlatListItems
            <Item
              navigation={this.props.navigation}
              id={item.id}            
              title={item.title}
              project={item}
             //selected={!!selected.get(item.id)}
             //onSelect={onSelect}
            />

          )}
          enableEmptySections={true}
          style={{ marginTop: 10 }}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
      </View>
      
    );
  }
}
 
const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: Constants.statusBarHeight,
  }
});