import React, { memo } from 'react';
import {
  ImageBackground,
  StyleSheet,
  KeyboardAvoidingView,
  View,
  Image
} from 'react-native';

const Background = ({ children }) => (
  <ImageBackground
    source={{uri:'https://realty.scopicafrica.com/react_native_assets/PwC_Rep_US_Oregon_ML_1194L_2.jpg'}}
    resizeMode="cover"
    style={styles.background}>
  <Image style={{backgroundColor: 'transparent',width:50,height:50,position:"absolute",borderRadius:10,right:10,top:10}}  
      source={{uri:'https://realty.scopicafrica.com/react_native_assets/pwc_logo_mono_outline_black_rgb_transparent.png'}}/>
    <KeyboardAvoidingView style={styles.container} behavior="padding">     
      {children}
    </KeyboardAvoidingView>
  </ImageBackground>
);

const styles = StyleSheet.create({
  background: {
    flex: 1,
    width: '100%',
  },
  container: {
    flex: 1,
    padding: 20,
    width: '100%',
    maxWidth: 340,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default memo(Background);