import React, { memo } from 'react';
import { Image, StyleSheet } from 'react-native';

const Logo = () => (
  <Image source={{uri:'https://realty.scopicafrica.com/react_native_assets/logo_emiradi_updated.png'}} style={styles.image} />

);


const styles = StyleSheet.create({
  image: {
    width: 128,
    height: 128,
    marginBottom: 12,
    borderRadius:10
  },
});

export default memo(Logo);