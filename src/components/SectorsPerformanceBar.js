import React from "react";
import { StyleSheet, View,Text,ScrollView } from "react-native";
import { VictoryBar,VictoryPie,VictoryArea,VictoryStack, VictoryChart, VictoryAxis,
        VictoryTheme } from 'victory-native';
const data = [
  { quarter: 1, earnings: 13000 },
  { quarter: 2, earnings: 16500 },
  { quarter: 3, earnings: 14250 },
  { quarter: 4, earnings: 19000 }
];
const sample_data=[
    { x: "Complete", y: 35 },
    { x: "Stalled", y: 40 },
    { x: "In progress", y: 55 },
    { x: "Not Started", y: 55 }
  ];

export default class App extends React.Component {
  render() {
    return (
        <VictoryStack theme={VictoryTheme.material}
  domainPadding={{ x: 10 }}  height={200}    horizontal="true" animate={{
  duration: 2000,
  onLoad: { duration: 1000 }
}}>

          <VictoryBar
            categories={{x: ["birds", "cats", "dogs"]}}
            
    data={[{x: "a", y: 2}, {x: "b", y: 3}, {x: "c", y: 5}]}
          />
          <VictoryBar
            data={[{x: "a", y: 1}, {x: "b", y: 4}, {x: "c", y: 5}]}
          />
          <VictoryBar
            data={[{x: "a", y: 3}, {x: "b", y: 2}, {x: "c", y: 6}]}
          />
        </VictoryStack>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#f5fcff"
  }
});