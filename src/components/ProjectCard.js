import React,{ Component } from 'react';
import {AsyncStorage,TouchableOpacity,Text,Image,View,StyleSheet,ProgressBarAndroid} from 'react-native';
import { Container, Header, Content, Card, CardItem, Thumbnail, Button, Icon, Left, Body, Right } from 'native-base';
import {NavigationActions} from 'react-navigation';
import { DataNavigation } from 'react-data-navigation';

export default class ProjectCard extends Component {
  
  

  render() {
    const project=this.props.project;
     const navigation=this.props.navigation;
    
    return (
      <View style={{paddingLeft:10,paddingRight:10}}>
       <Card>
            <CardItem>
              <Left>
                <Thumbnail source={{uri: "https://ui-avatars.com/api/?name="+project.project_manager.first_name+" "+project.project_manager.last_name}} />
                <Body>
                  <Text>{this.props.project.title}</Text>
                  <Text note>{project.project_manager.first_name} {project.project_manager.last_name} (Manager)</Text>
                </Body>
              </Left>
            </CardItem>
            <CardItem cardBody>
              <Image source={{uri:"https://portal.epesicloud.com/"+project.featured_photo}} style={{height: 200, width: null, flex: 1}}/>
            </CardItem>
            <CardItem>
                    <Body>
                        <Text>
                           {project.description}
                        </Text>
                    </Body>
            </CardItem> 
            <View style={{paddingHorizontal:20,paddingVertical:10}}>
              <View style={{flexDirection:"row",justifyContent:"space-between"}} >

                <View>
                  <Text style={styles.smallText}>{project.progress}% Complete</Text>
                </View>

                <View>
                <TouchableOpacity transparent  
                onPress={function(){
                  DataNavigation.setData("project",project);  
                  DataNavigation.setData("activities_count",project.activities.length);                 
                  navigation.navigate('ProjectDashboardTab', {}, 
                  NavigationActions.navigate({ routeName: 'ProjectDashboardScreen',params:{activities_count:project.activities.length,activities:project.activities,project:project} }))
                }}> 
                      <Text style={styles.smallText} >View Project</Text>
                    </TouchableOpacity>
                    
                </View>
                
              </View>

              
              <View style={{flexDirection:"row",justifyContent:"space-between"}} >

                <View>
                    <ProgressBarAndroid
                      styleAttr="Horizontal"
                      indeterminate={false}
                      progress={parseFloat(project.progress)}
                    />
                  </View>

                  <View>
                    <Text style={{color:"#ccc"}}>{project.activities.length} Activities</Text>
                  </View>              
                  
                  
                
                
              </View>
              
            </View>
          </Card>

    
    
    </View>
    );
  }
}

const styles = StyleSheet.create({
  
  smallText: {
    fontSize: 12,
    fontWeight: 'bold',
  },
});