import React, { Component } from 'react';
import { Container, Header, Content, List, ListItem, Thumbnail, Left, Body, Right, Button } from 'native-base';
import {View,Text,ProgressBarAndroid} from 'react-native';
import ActivityProgressBar from './ActivityProgressBar';
export default class ActivityList extends Component {
  render() {
    let count=0;
   
   const navigation=this.props.navigation;
    if(this.props.activities)
    {
      let activities_items = this.props.activities.map(function(item, key){
        count++;
        
        return (
        <ListItem thumbnail>
          <Body>
        <ActivityProgressBar navigation={navigation} activity_count={count} activity={item}/>
          </Body>
        </ListItem>);
      }
      );
      return (
        <List>
          {activities_items}
        </List>
      );
    }
    else
    {
      return (
        <List>
          <ListItem thumbnail>
          <Left>
            
          </Left>
          <Body>
            <Text>(0) Activities  {this.props.status} </Text>
            <Text note numberOfLines={2}> There are currently no activities {this.props.status}</Text>
            
          </Body>
          <Right>
            
          </Right>
        </ListItem>
        </List>
      );
    }
    
  }
}