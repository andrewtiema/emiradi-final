import React from "react";
import { StyleSheet, View,Text,ScrollView } from "react-native";
import { VictoryBar,VictoryPie,VictoryLabel,VictoryArea,VictoryStack, VictoryChart, VictoryAxis,
        VictoryTheme } from 'victory-native';
const data = [
  { quarter: 1, earnings: 13000 },
  { quarter: 2, earnings: 16500 },
  { quarter: 3, earnings: 14250 },
  { quarter: 4, earnings: 19000 }
];
const sample_data=[
    { x: "Complete", y: 35 },
    { x: "In progress", y: 55 },
    { x: "Stalled", y: 40 },
    { x: "Not Started", y: 55 }
  ];

export default class ProjectPerformancePie extends React.Component {
  render() {
    let status=this.props.status;
    let status_data=[];
   

    let not_started_percent=Math.round((this.props.status.not_started/this.props.status.total)*100);
    let ongoing_percent=Math.round((this.props.status.ongoing/this.props.status.total)*100);
    let completed_percent=Math.round((this.props.status.completed/this.props.status.total)*100)
    let stalled_percent=Math.round((this.props.status.stalled/this.props.status.total)*100);
    
    if(status.not_started)
    {
      status_data.push({x:"Not Started \n ("+not_started_percent+"%)",y:this.props.status.not_started});
    }

    if(status.ongoing)
    {
      status_data.push({x:"Ongoing \n ("+ongoing_percent+"%)",y:this.props.status.ongoing});
    }

    if(status.completed)
    {
      status_data.push({x:"Complete \n ("+completed_percent+"%)",y:this.props.status.completed});
    }

    if(status.stalled)
    {
      status_data.push({x:"Stalled \n ("+stalled_percent+"%)",y:this.props.status.stalled});
    }


    return (
      <VictoryPie colorScale={["yellow", "#ffcc00","#92d050", "#ff0000", "navy" ]} animate={{
        duration: 3000
      }} width="350" theme={VictoryTheme.material}
                innerRadius={40}
                data={status_data}
                labelPosition="centroid"
                labelComponent={<VictoryLabel angle={0}/>}
                labelRadius={({ innerRadius }) => innerRadius + 75 }
                radius={100}
                padAngle={2} 
        />
    );
  }
}