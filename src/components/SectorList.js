import React, { Component } from 'react';
import { Container, Header, Content, List, ListItem, Thumbnail, Text, Left, Body, Right, Button } from 'native-base';
import {ProgressBarAndroid} from 'react-native';
export default class SectorList extends Component {
  render() {
    return (
      <List>
            <ListItem thumbnail>
              <Left>
                <Thumbnail square source={{ uri: 'https://constructionreviewonline.com/wp-content/uploads/2019/03/road.jpg' }} />
              </Left>
              <Body>
                <Text>1. Road Preparation </Text>
                <Text note numberOfLines={2}>Its time to build a difference (50% Complete)</Text>
                <ProgressBarAndroid
          styleAttr="Horizontal"
          indeterminate={false}
          progress={0.5}
        /> 
              </Body>
              <Right>
                <Button transparent>
                  <Text>View</Text>
                </Button>
              </Right>
            </ListItem>

            <ListItem thumbnail>
              <Left>
                <Thumbnail square source={{ uri: 'https://constructionreviewonline.com/wp-content/uploads/2019/06/Road_construction_in_Bayelsa_State.jpg' }} />
              </Left>
              <Body>
                <Text>2.Road Surfacing</Text>
                <Text note numberOfLines={2}>Its time to build a difference (20% Complete)</Text>
                <ProgressBarAndroid
          styleAttr="Horizontal"
          indeterminate={false}
          progress={0.5}
        /> 
              </Body>
              <Right>
                <Button transparent>
                  <Text>View</Text>
                </Button>
              </Right>
            </ListItem>
            <ListItem thumbnail>
              <Left>
                <Thumbnail square source={{ uri: 'https://constructionreviewonline.com/wp-content/uploads/2019/03/road.jpg' }} />
              </Left>
              <Body>
                <Text>3. Road Construction </Text>
                <Text note numberOfLines={2}>Its time to build a difference (50% Complete)</Text>
                <ProgressBarAndroid
          styleAttr="Horizontal"
          indeterminate={false}
          progress={0.5}
        /> 
              </Body>
              <Right>
                <Button transparent>
                  <Text>View</Text>
                </Button>
              </Right>
            </ListItem>

            <ListItem thumbnail>
              <Left>
                <Thumbnail square source={{ uri: 'https://constructionreviewonline.com/wp-content/uploads/2019/06/Road_construction_in_Bayelsa_State.jpg' }} />
              </Left>
              <Body>
                <Text>4.Road Construction1</Text>
                <Text note numberOfLines={2}>Its time to build a difference (20% Complete)</Text>
                <ProgressBarAndroid
          styleAttr="Horizontal"
          indeterminate={false}
          progress={0.5}
        /> 
              </Body>
              <Right>
                <Button transparent>
                  <Text>View</Text>
                </Button>
              </Right>
            </ListItem>
          </List>
    );
  }
}