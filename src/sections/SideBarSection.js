import React, { Component } from "react";
import { Platform, Dimensions,Image,ImageBackground } from "react-native";
import {
  View,
  Content,
  Text,
  List,
  ListItem,
  Icon,
  Container,
  Left,
  Right,
  Badge
} from "native-base";
import {Ionicons} from "react-native-vector-icons"


const drawerCover ={"uri":"https://realty.scopicafrica.com/react_native_assets/drawer-cover.png"};
const drawerImage = {"uri":"https://realty.scopicafrica.com/react_native_assets/logo-kitchen-sink.png"};

const datas = [
  {
    name: "Home",
    route: "DashboardStack",
    icon: "home",
    bg: "#5DCEE2"
  },
  {
    name: "Explore",
    route: "ProjectsStack",
    icon: "albums",
    bg: "#EB6B23"
   // types: "6"
  },
 /** {
    name: "Notifications",
    route: "NotificationsStack",
    icon: "notifications",
    bg: "#cc0000",
    types: "3"
  },**/
  {
    name: "Gallery",
    route: "GalleryStack",
    icon: "image",
    bg: "#cc0000"
  },
  {
    name: "Users",
    route: "ProjectUsersStack",
    icon: "people",
    bg: "#EFB406"
  },
  {
    name: "My Profile",
    route: "MyProfileStack",
    icon: "calendar",
    bg: "#EFB406"
  },
  {
    name: "Settings",
    route: "SettingsStack",
    icon: "settings",
    bg: "#EFB406"
  },
  {
    name: "Logout",
    route: "AuthStack",
    icon: "lock",
    bg: "#5DCEE2"
  },
  
  
  
];

class SideBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      shadowOffsetWidth: 1,
      shadowRadius: 4
    };
  }

  render() {
    return (
      <Container>
        <Content
          bounces={false}
          style={{ flex: 1, backgroundColor: "#fff", top: -1 }}
        >
        
          <Image source={drawerCover} style={styles.drawerCover} />
          <Image square style={styles.drawerImage} source={drawerImage} />
         
         
          <List
            dataArray={datas}
            renderRow={data =>
              <ListItem
                button
                noBorder
                onPress={() => this.props.navigation.navigate(data.route)}
              >
                <Left>
                  <Icon
                    active
                    name={data.icon}
                    style={{ color: "#777", fontSize: 26, width: 30 }}
                  />
                  <Text style={styles.text}>
                    {data.name}
                  </Text>
                </Left>
                {data.types &&
                  <Right style={{ flex: 1 }}>
                    <Badge
                      style={{
                        borderRadius: 3,
                        height: 25,
                        width: 25,
                        backgroundColor: data.bg
                      }}
                    >
                      <Text
                        style={styles.badgeText}
                      >{`${data.types}`}</Text>
                    </Badge>
                  </Right>}
              </ListItem>}
          />
         
        </Content>
      </Container>
    );
  }
}
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

var styles= {
  drawerCover: {
    alignSelf: "stretch",
    height: deviceHeight / 3.5,
    width: null,
    position: "relative",
    marginBottom: 10
  },
  drawerImage: {
    position: "absolute",
    left: Platform.OS === "android" ? deviceWidth / 10 : deviceWidth / 9,
    top: Platform.OS === "android" ? deviceHeight / 13 : deviceHeight / 12,
    width: 210,
    height: 75,
    resizeMode: "cover"
  },
  text: {
    fontWeight: Platform.OS === "ios" ? "500" : "400",
    fontSize: 16,
    marginLeft: 20
  },
  badgeText: {
    fontSize: Platform.OS === "ios" ? 13 : 11,
    fontWeight: "400",
    textAlign: "center",
    marginTop: Platform.OS === "android" ? -3 : undefined
  }
};
export default SideBar;